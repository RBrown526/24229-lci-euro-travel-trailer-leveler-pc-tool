// IDS.BarCodeReader.cs
// Version 1.2

// History
// 1.0 - Initial release
// 1.1 - No longer requires "Keyboard Hook.dll"
// 1.2 - to ensure compatibility with bar code reader, the code
//       checks for the "return" key at the end of the scan

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Text;
using System.Runtime.InteropServices; // dll inclusion

namespace IDS
{
    // keystroke match strings contain characters to be matched
    // - all keystrokes come in as upper case letters
    // - to match a literal character, use the upper case version
    // - lower case letters operate as the following
    //    a = alphanumeric (A-Z, 0-9)
    //    d = digit (0-9)
    //    l = letter (Z-A)
    //    x = any keystroke
    static class BarCodeReader
    {
        public delegate void BarCodeCallback(String match, String text);

        // class that matches keystrokes to a known pattern
        class KeyStrokeMatcher
        {
            BarCodeCallback Callback;
            String Match;
            char[] Buffer;
            int MatchIndex = 0;
            int BufferIndex = 0;
            UInt32 KeyTimeout = 500; // 1/2 second between key strokes
            bool ExactLength = false;
            bool overrun = false;

            public KeyStrokeMatcher(String match, BarCodeCallback callback)
            {
                Callback = callback;
                Match = match;
                Buffer = new char[Match.Length];
                ExactLength = false;
            }

            public KeyStrokeMatcher(String match, double timeout, BarCodeCallback callback)
            {
                Callback = callback;
                Match = match;
                Buffer = new char[Match.Length];
                KeyTimeout = (UInt32)(timeout * 1000 + 0.5);
                ExactLength = false;
            }

            public KeyStrokeMatcher(String match, bool extactlength, BarCodeCallback callback)
            {
                Callback = callback;
                Match = match;
                Buffer = new char[Match.Length];
                ExactLength = extactlength;
            }

            public KeyStrokeMatcher(String match, double timeout, bool extactlength, BarCodeCallback callback)
            {
                Callback = callback;
                Match = match;
                Buffer = new char[Match.Length];
                KeyTimeout = (UInt32)(timeout * 1000 + 0.5);
                ExactLength = extactlength;
            }

            bool IsNextKeyInMatch(char c)
            {
                switch (Match[MatchIndex])
                {
                    case 'a': // alphanumeric (A-Z, 0-9)
                        if (c >= '0' && c <= '9')
                            break;
                        if (c >= 'A' && c <= 'Z')
                            break;
                        if (c >= 'a' && c <= 'z')
                            break;
                        return false;
                    case 'd': // digit (0-9)
                        if (c >= '0' && c <= '9')
                            break;
                        return false;
                    case 'l': // letter (A-Z)
                        if (c >= 'A' && c <= 'Z')
                            break;
                        if (c >= 'a' && c <= 'z')
                            break;
                        return false;
                    case 'x': // any keystroke
                        break;
                    default: // literal match
                        if (Match[MatchIndex] == c)
                            break;
                        return false;
                }
                MatchIndex++;
                return true;
            }

            bool KeepKeystroke(char c)
            {
                if (ExactLength && overrun)
                    return false;

                if (MatchIndex >= Match.Length)
                    return true;

                for (; ; )
                {
                    // is this the key we are looking for?
                    if (IsNextKeyInMatch(c))
                        return true; // keep the key

                    // we don't match
                    // however, we might be able to recover
                    // example:  match = ABCABG
                    //           typed = ABCABC
                    //           we can restart at second A

                    // anything to recover?
                    if (MatchIndex <= 0)
                        return false; // no

                    // attempt recovery
                    int key_count = MatchIndex;
                    MatchIndex = 0;
                    BufferIndex = 0;
                    for (int i = 1; i < key_count; i++)
                    {
                        char prev_key = Match[i];
                        if (!IsNextKeyInMatch(prev_key))
                        {
                            MatchIndex = 0;
                            BufferIndex = 0;
                        }
                        else if (BufferIndex < Buffer.Length)
                            Buffer[BufferIndex++] = prev_key;
                    }
                    // now continue and check the current keystroke
                }
            }

            public void OnKeystroke(char c, UInt32 delta)
            {
                // abort if key timeout expires
                if (delta > KeyTimeout)
                {
                    MatchIndex = 0;
                    BufferIndex = 0;
                    overrun = false;
                }

                // return key
                if (c == 13)
                {
                    // alert owner if we have a match, and the buffer is full
                    if (MatchIndex >= Match.Length && BufferIndex >= Buffer.Length)
                    {
                        if (!ExactLength || !overrun)
                        {
                            String m = Match;
                            String b = new String(Buffer);
                            Hook.Form.BeginInvoke((MethodInvoker)delegate
                            {
                                Callback(m, b);
                            });
                        }
                    }
                    MatchIndex = 0;
                    BufferIndex = 0;
                    overrun = false;
                    return;
                }

                // if buffer is full, but we did not get return key
                if (MatchIndex >= Match.Length && BufferIndex >= Buffer.Length)
                {
                    // clear the buffer and start over
                    MatchIndex = 0;
                    BufferIndex = 0;
                    overrun = true;
                }

                // are we still looking for a match?
                if (!KeepKeystroke(c))
                    return;

                // keep the keystroke
                if (BufferIndex < Buffer.Length)
                    Buffer[BufferIndex++] = c;

#if false
                // alert owner if we have a match, and the buffer is full
                if (MatchIndex >= Match.Length && BufferIndex >= Buffer.Length)
                {
                    Callback(Match, new String(Buffer));
                    MatchIndex = 0;
                    BufferIndex = 0;
                }
#endif
            }
        }

#if true
        static class Hook
        {
            const int WH_KEYBOARD_LL = 13;
            const int WM_KEYDOWN = 0x0100;
            const int WM_KEYUP = 0x0101;

            delegate int keyboardHookProc(int code, int wParam, ref keyboardHookStruct lParam);

            [DllImport("user32.dll")]
            static extern IntPtr SetWindowsHookEx(int idHook, keyboardHookProc callback, IntPtr hInstance, uint threadId);
            [DllImport("user32.dll")]
            static extern bool UnhookWindowsHookEx(IntPtr hInstance);
            [DllImport("user32.dll")]
            static extern int CallNextHookEx(IntPtr idHook, int nCode, int wParam, ref keyboardHookStruct lParam);
            [DllImport("kernel32.dll")]
            static extern IntPtr LoadLibrary(string lpFileName);
            [DllImport("user32.dll")]
            static extern int ToAscii(int uVirtKey, int uScanCode, byte[] lpbKeyState, byte[] lpChar, int uFlags);
            [DllImport("user32.dll")]
            static extern int GetKeyboardState(byte[] pbKeyState);
            [DllImport("user32.dll")]
            static extern uint MapVirtualKey(int uCode, uint uMapType);

            public struct keyboardHookStruct
            {
                public int vkCode;
                public int scanCode;
                public int flags;
                public int time;
                public int dwExtraInfo;
            }

            static object CriticalSection = new object();
            static public KeyCaptureForm Form = null;
            static IntPtr hHook = IntPtr.Zero;
            static keyboardHookProc Callback = new keyboardHookProc(KeyboardProc);

            static public bool Attach(KeyCaptureForm form)
            {
                lock (CriticalSection)
                {
                    if (hHook == IntPtr.Zero)
                    {
                        Form = form;
                        IntPtr p = Form.Handle; // create the form

                        IntPtr hInstance = LoadLibrary("User32");
                        hHook = SetWindowsHookEx(WH_KEYBOARD_LL, Callback, hInstance, 0);

                        return hHook != IntPtr.Zero;
                    }
                }

                return false;
            }

            static public void Detach()
            {
                lock (CriticalSection)
                {
                    if (hHook != IntPtr.Zero)
                    {
                        UnhookWindowsHookEx(hHook);
                        hHook = IntPtr.Zero;
                    }
                }
            }

            static public int KeyboardProc(int code, int wParam, ref keyboardHookStruct lParam)
            {
#if false
                if (code == 0)
                {
                    System.Diagnostics.Debug.WriteLine(
                        GetAsciiCharacter(lParam.vkCode, lParam.scanCode)
                        + " " + wParam.ToString()
                        + " " + lParam.vkCode.ToString()
                        + " " + lParam.scanCode.ToString()
                        + " " + lParam.flags.ToString());

                }
#endif
                if (code == 0 && wParam == WM_KEYDOWN)
                {
                    //                    Form.OnKeystroke((char)MapVirtualKey(lParam.vkCode, 2));
                    if (lParam.vkCode != 13)
                        Form.OnKeystroke(GetAsciiCharacter(lParam.vkCode, lParam.scanCode));
                }
                if (code == 0 && wParam == WM_KEYUP && lParam.vkCode == 13)
                    Form.OnKeystroke((char)13);

                return CallNextHookEx(hHook, code, wParam, ref lParam);
            }

            static byte[] lpKeyState = new byte[256];
            static byte[] lpChar = new byte[2];
            static char GetAsciiCharacter(int uVirtKey, int uScanCode)
            {
                GetKeyboardState(lpKeyState);
                if (ToAscii(uVirtKey, uScanCode, lpKeyState, lpChar, 0) == 1)
                    return (char)lpChar[0];
                else
                    return (char)0;
            }
        }
#else
        static class Hook
        {
            const int WH_CALLWNDPROC = 4;
            const int WM_CHAR = 0x0102;

            delegate int HookProc(int code, int wParam, int lParam);

            [DllImport("user32.dll")]
            static extern IntPtr SetWindowsHookEx(int idHook, HookProc callback, IntPtr hInstance, uint threadId);
            [DllImport("user32.dll")]
            static extern bool UnhookWindowsHookEx(IntPtr hInstance);
            [DllImport("user32.dll")]
            static extern int CallNextHookEx(IntPtr idHook, int nCode, int wParam, int lParam);
            [DllImport("kernel32.dll")]
            static extern IntPtr LoadLibrary(string lpFileName);
            [DllImport("user32.dll")]
            static extern int ToAscii(int uVirtKey, int uScanCode, byte[] lpbKeyState, byte[] lpChar, int uFlags);
            [DllImport("user32.dll")]
            static extern int GetKeyboardState(byte[] pbKeyState);

            static object CriticalSection = new object();
            static KeyCaptureForm Form = null;
            static IntPtr hHook = IntPtr.Zero;

            static public bool Attach(KeyCaptureForm form)
            {
                lock (CriticalSection)
                {
                    if (hHook == IntPtr.Zero)
                    {
                        Form = form;
                        IntPtr p = Form.Handle; // create the form

                        IntPtr hInstance = LoadLibrary("User32");
                        hHook = SetWindowsHookEx(WH_CALLWNDPROC, CallWndProc, hInstance, 0);

                        return hHook != IntPtr.Zero;
                    }
                }

                return false;
            }

            static public void Detach()
            {
                lock (CriticalSection)
                {
                    if (hHook != IntPtr.Zero)
                    {
                        UnhookWindowsHookEx(hHook);
                        hHook = IntPtr.Zero;
                    }
                }
            }

            [StructLayout(LayoutKind.Sequential)]
            public struct CWPRETSTRUCT
            {
                public IntPtr lResult;
                public IntPtr lParam;
                public IntPtr wParam;
                public uint message;
                public IntPtr hwnd;
            } ;

            static public int CallWndProc(int code, int wParam, int lParam)
            {
                if (code == 0)
                {
                    CWPRETSTRUCT msg = (CWPRETSTRUCT)Marshal.PtrToStructure((IntPtr)lParam, typeof(CWPRETSTRUCT));
                    if (msg.message == WM_CHAR)
                    {
                        Form.OnKeystroke((char)msg.wParam);
                    }
                }
                return CallNextHookEx(hHook, code, wParam, lParam);
            }

            static byte[] lpKeyState = new byte[256];
            static byte[] lpChar = new byte[2];
            static char GetAsciiCharacter(int uVirtKey, int uScanCode)
            {
                GetKeyboardState(lpKeyState);
                if (ToAscii(uVirtKey, uScanCode, lpKeyState, lpChar, 0) == 1)
                    return (char)lpChar[0];
                else
                    return (char)0;
            }
        }
#endif

        // window that captures keystrokes from the bar code reader
        class KeyCaptureForm : Form
        {
            public bool Ready = false;

            public KeyCaptureForm()
            {
                try
                {
                    Ready = Hook.Attach(this);
                }
                catch
                {
                }

                if (!Ready)
                {
                    MessageBox.Show("Unable to initialize bar code reader!", "IDS Bar Code Reader", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

            ~KeyCaptureForm()
            {
                try
                {
                    Hook.Detach();
                }
                catch
                {
                }
            }

            // keystroke time management
            [DllImport("winmm.dll")]
            static extern UInt32 timeGetTime();
            bool FirstKey = true;
            UInt32 Time;

            internal void OnKeystroke(char c)
            {
                // only process printable ascii characters, and the return key
                if (c == 13 || (c >= 0x20 && c <= 0x7E))
                {
                    // force all letters to uppercase
                    if (c >= 'a' && c <= 'z')
                        c += unchecked((char)('A' - 'a'));

                    // measure time between keystrokes
                    UInt32 delta = Time;
                    Time = timeGetTime();
                    delta = Time - delta;
                    if (FirstKey)
                    {
                        FirstKey = false;
                        delta = 0;
                    }

                    //                    BeginInvoke((MethodInvoker)delegate
                    //                    {
                    //                        System.Diagnostics.Debug.WriteLine(c);

                    foreach (KeyStrokeMatcher matcher in List)
                        matcher.OnKeystroke(c, delta);
                    //                    });
                }
            }
        }

        static List<KeyStrokeMatcher> List = new List<KeyStrokeMatcher>();
        static KeyCaptureForm TheForm = new KeyCaptureForm();
        static public bool Ready { get { return TheForm.Ready; } }

        static public void Add(String match, BarCodeCallback callback)
        {
            List.Add(new KeyStrokeMatcher(match, callback));
        }

        static public void Add(String match, double timeout, BarCodeCallback callback)
        {
            List.Add(new KeyStrokeMatcher(match, timeout, callback));
        }

        static public void Add(String match, bool exactlength, BarCodeCallback callback)
        {
            List.Add(new KeyStrokeMatcher(match, exactlength, callback));
        }

        static public void Add(String match, double timeout, bool exactlength, BarCodeCallback callback)
        {
            List.Add(new KeyStrokeMatcher(match, timeout, exactlength, callback));
        }
    }
}
