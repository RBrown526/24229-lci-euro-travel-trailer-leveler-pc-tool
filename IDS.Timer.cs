// IDS.Timer.cs
// Version 1.2
//
// History
// 1.0 -- first release
// 1.1 -- performance improvement (System.Security.SuppressUnmanagedCodeSecurity)
// 1.2 -- added Adjust()
//
//
// Class for performing timing measurements, and generating timing events
//
// IDS.Timer
// - class that provides timing measurement
// - Reset() resets the timer
// - Elapsed provides the time elapsed (in seconds) since the last Reset()
//
// IDS.Timer.Event
// - class that provides

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.InteropServices; // dll import
using System.Threading;

namespace IDS
{
    class Timer
    {
        public class Event
        {
            public delegate void Handler();

            static class Win32
            {
                public delegate void TimerEventHandler(uint id, uint msg, ref int userCtx, int rsv1, int rsv2);

                [System.Security.SuppressUnmanagedCodeSecurity] // We won't use this maliciously
                [DllImport("WinMM.dll", SetLastError = true)]
                public static extern uint timeSetEvent(uint uDelay, uint uResolution, TimerEventHandler lpFunction, IntPtr dwUser, Type uFlags);

                [System.Security.SuppressUnmanagedCodeSecurity] // We won't use this maliciously
                [DllImport("WinMM.dll", SetLastError = true)]
                public static extern uint timeKillEvent(uint timerEventId);
            }

            public enum Type : uint
            {
                OneShot = 0x0000, /* program timer for single event */
                Periodic = 0x0001 /* program for continuous periodic event */
            }

            uint ID = 0;
            uint mPeriod = 1000;
            uint mResolution = 15;
            Type mEventType = Type.Periodic;
            Handler Callback = null; // user can set a callback function
            EventWaitHandle EventObj = null; // user can have an event set
            Win32.TimerEventHandler TimerCallback = null; // set to appropriate delegate

            public uint Period
            {
                get { return mPeriod; }
                set
                {
                    if (value < 1)
                        value = 1;
                    if (mPeriod != value)
                    {
                        mPeriod = value;
                        if (Active)
                            Start(); // restart
                    }
                }
            }

            public uint Resolution
            {
                get { return mResolution; }
                set
                {
                    if (value < 1)
                        value = 1;
                    if (mResolution != value)
                    {
                        mResolution = value;
                        if (Active)
                            Start(); // restart
                    }
                }
            }
            
            public Type EventType
            {
                get { return mEventType; }
                set
                {
                    if (mEventType != value)
                    {
                        mEventType = value;
                        if (Active)
                            Start(); // restart
                    }
                }
            }

            public bool Active
            {
                get { return ID != 0; }
                set
                {
                    if (Active != value)
                    {
                        if (value)
                            Start();
                        else
                            Stop();
                    }
                }
            }

            public Event(Type type, Handler callback)
            {
                mEventType = type;
                Callback = callback;
                if (type == Type.OneShot)
                    TimerCallback = DoCallbackOneShot;
                else
                    TimerCallback = DoCallback;
            }
            
            public Event(Type type, EventWaitHandle handle)
            {
                mEventType = type;
                EventObj = handle;
                if (type == Type.OneShot)
                    TimerCallback = SetEventOneShot;
                else
                    TimerCallback = SetEvent;
            }

            ~Event()
            {
                Stop();
            }

            public void Start()
            {
                Stop();
                ID = Win32.timeSetEvent(Period, Resolution, TimerCallback, IntPtr.Zero, EventType);
            }

            public void Stop()
            {
                if (ID != 0)
                    Win32.timeKillEvent(ID);
                ID = 0;
            }

            // callback that executes the user callback
            void DoCallback(uint id, uint msg, ref int userCtx, int rsv1, int rsv2)
            {
                Callback();
            }

            // callback that sets the user event
            void SetEvent(uint id, uint msg, ref int userCtx, int rsv1, int rsv2)
            {
                EventObj.Set();
            }

            // callback that executes the user callback
            void DoCallbackOneShot(uint id, uint msg, ref int userCtx, int rsv1, int rsv2)
            {
                Callback();
                Stop();
            }

            // callback that sets the user event
            void SetEventOneShot(uint id, uint msg, ref int userCtx, int rsv1, int rsv2)
            {
                EventObj.Set();
                Stop();
            }
        }

        static class Win32
        {
            [System.Security.SuppressUnmanagedCodeSecurity] // We won't use this maliciously
            [DllImport("winmm.dll")]
            public static extern uint timeGetTime();

            [System.Security.SuppressUnmanagedCodeSecurity] // We won't use this maliciously
            [DllImport("winmm.dll")]
            public static extern uint timeBeginPeriod(uint period);

            [System.Security.SuppressUnmanagedCodeSecurity] // We won't use this maliciously
            [DllImport("winmm.dll")]
            public static extern uint timeEndPeriod(uint period);
        }

        uint Resolution = 0;
        uint Time;

        public Timer()
        {
            Reset();
        }

        public Timer(uint resolution)
        {
            Resolution = resolution;
            if (Resolution > 0)
                Win32.timeBeginPeriod(Resolution);
            Reset();
        }

        ~Timer()
        {
            if (Resolution > 0)
                Win32.timeEndPeriod(Resolution);
        }

        public void Reset()
        {
            Time = Win32.timeGetTime();
        }

        public void Adjust(int seconds)
        {
            Time += (uint)(int)(seconds * 1000);
        }

        public void Adjust(float seconds)
        {
            Time += (uint)(int)(seconds * 1000);
        }

        public void Adjust(double seconds)
        {
            Time += (uint)(int)(seconds * 1000);
        }

        public double Elapsed
        {
            get
            {
                return 0.001 * (uint)(Win32.timeGetTime() - Time);
            }
        }
    }
}
