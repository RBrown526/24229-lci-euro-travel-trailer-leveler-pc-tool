using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace _21602_LCI_myRV_TT_Leveler_SE_ICT
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}