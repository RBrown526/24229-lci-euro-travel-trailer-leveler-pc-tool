using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IDS
{
    // class that automatically creates a image buffer for a System.Windows.Forms.Control
    // the image buffer is automatically resized with the control
    // the image buffer can be used as the back buffer, simulating a "double buffered" control
    class ControlBackBuffer
    {
        Control Owner = null; // control that owns us (we determine image size from this control)
        Bitmap mBitmap = null; // the buffered image
        Graphics mGraphics = null; // graphics context for the image

        public ControlBackBuffer(Control owner)
        {
            // link into owner
            Owner = owner;
            Owner.Resize += new System.EventHandler(this.OnResize);
        }

        // when control is resized, destroy all objects (their size is wrong)
        // the objects will be re-created when needed
        void OnResize(object sender, EventArgs e)
        {
            if (mGraphics != null)
            {
                mGraphics.Dispose();
                mGraphics = null;
            }

            if (mBitmap != null)
            {
                mBitmap.Dispose();
                mBitmap = null;
            }
        }

        // get the image (create it as necessary)
        public Bitmap Image
        {
            get
            {
                if (mBitmap == null)
                    mBitmap = new Bitmap(Owner.ClientSize.Width, Owner.ClientSize.Height);
                return mBitmap;
            }
        }

        // get the graphics context (create it as necessary)
        public Graphics Graphics
        {
            get
            {
                if (mGraphics == null)
                    mGraphics = Graphics.FromImage(Image);
                return mGraphics;
            }
        }
    }
}
