// IDS.AboutBox.cs
// Version 1.2
//
// History
// 1.0 -- first release
// 1.1 -- minor update
// 1.2 -- minor update

using Microsoft.Win32;    // Required for the registry classes.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace IDS
{
    public partial class AboutBox : IDS.Form
    {
        public AboutBox()
        {
            InitializeComponent();

            // set the icon to the default application icon
            PictureBox.Image = IDS.AppInfo.GetIcon(PictureBox.Width, PictureBox.Height).ToBitmap();

            // get application title
            AppTitle.Text = IDS.AppInfo.Title;
            MinWidth = AppTitle.Left + AppTitle.Width + PictureBox.Left;

            // get application version
            AppVersion.Text =
                "Version: " + IDS.AppInfo.Version.ToString() + "\n"
                + "Build date: " + IDS.AppInfo.BuildDate.ToString("MMMM dd yyyy hh:mm tt");
            MinWidth = AppVersion.Left + AppVersion.Width + PictureBox.Left;

            // get application copyright, etc...
            AppInfo.Text =
                IDS.AppInfo.Copyright + "\n"
                + "\n"
                + IDS.AppInfo.CompanyName + "\n\n";
            MinWidth = AppInfo.Left + AppInfo.Width + PictureBox.Left;

            // resize height to fit text
            int nch = Height - ClientRectangle.Height;
            Height = nch + AppInfo.Top + AppInfo.Height + Legal.Height + PictureBox.Top;
        }

        int MinWidth
        {
            set
            {
                if (Width < value)
                    Width = value;
            }
        }

        private void SystemInfoButton_Click(object sender, EventArgs e)
        {
            string strSysInfo = string.Empty;
            if (GetMsinfo32Path(ref strSysInfo))
            {
                try
                {
                    Process.Start(strSysInfo);
                }
                catch (Win32Exception ex)
                {
                    MessageBox.Show(this, ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        bool GetMsinfo32Path(ref string strPath)
        {
            strPath = string.Empty;
            object objTmp = null;
            RegistryKey regKey = Registry.LocalMachine;

            if (regKey != null)
            {
                regKey = regKey.OpenSubKey("Software\\Microsoft\\Shared Tools\\MSInfo");
                if (regKey != null)
                    objTmp = regKey.GetValue("Path");

                if (objTmp == null)
                {
                    regKey = regKey.OpenSubKey("Software\\Microsoft\\Shared Tools Location");
                    if (regKey != null)
                    {
                        objTmp = regKey.GetValue("MSInfo");
                        if (objTmp != null)
                            strPath = Path.Combine(
                               objTmp.ToString(), "MSInfo32.exe");
                    }
                }
                else
                    strPath = objTmp.ToString();

                try
                {
                    FileInfo fi = new FileInfo(strPath);
                    return fi.Exists;
                }
                catch (ArgumentException)
                {
                    strPath = string.Empty;
                }
            }

            return false;
        }

        private void AboutBox_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible && Owner != null)
            {
                if (typeof(IDS.Form).IsAssignableFrom(Owner.GetType()))
                {
                    BackgroundBrush.Color1 = ((IDS.Form)Owner).BackgroundBrush.Color1;
                    BackgroundBrush.Color2 = ((IDS.Form)Owner).BackgroundBrush.Color2;
                    BackgroundBrush.Angle = ((IDS.Form)Owner).BackgroundBrush.Angle;
                    BackgroundBrush.Mode = ((IDS.Form)Owner).BackgroundBrush.Mode;
                    ForeColor = ((IDS.Form)Owner).ForeColor;
                }
                else
                {
                    BackgroundBrush.Color1 = Owner.BackColor;
                    BackgroundBrush.Color2 = Color.Black;
                    ForeColor = Owner.ForeColor;
                }
            }
        }
    }
}