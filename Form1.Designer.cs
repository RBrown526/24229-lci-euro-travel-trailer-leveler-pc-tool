namespace _21602_LCI_myRV_TT_Leveler_SE_ICT
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.UserButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SerialNumberLabel = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.PortStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.DUTStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.HistoryBox = new System.Windows.Forms.ListBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StatusIconLabel = new System.Windows.Forms.Label();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.TesterStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetTesterCycleCountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StatusBar = new System.Windows.Forms.Panel();
            this.ProgressBar = new IDS.ProgressBar();
            this.statusStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // UserButton
            // 
            this.UserButton.Enabled = false;
            this.UserButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserButton.Location = new System.Drawing.Point(22, 103);
            this.UserButton.Name = "UserButton";
            this.UserButton.Size = new System.Drawing.Size(238, 57);
            this.UserButton.TabIndex = 0;
            this.UserButton.Text = "START";
            this.UserButton.UseVisualStyleBackColor = true;
            this.UserButton.Visible = false;
            this.UserButton.Click += new System.EventHandler(this.UserButton_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SerialNumberLabel
            // 
            this.SerialNumberLabel.AutoSize = true;
            this.SerialNumberLabel.BackColor = System.Drawing.Color.Transparent;
            this.SerialNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SerialNumberLabel.Location = new System.Drawing.Point(17, 34);
            this.SerialNumberLabel.Name = "SerialNumberLabel";
            this.SerialNumberLabel.Size = new System.Drawing.Size(151, 25);
            this.SerialNumberLabel.TabIndex = 1;
            this.SerialNumberLabel.Text = "Serial number:";
            // 
            // StatusLabel
            // 
            this.StatusLabel.BackColor = System.Drawing.Color.Transparent;
            this.StatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusLabel.Location = new System.Drawing.Point(147, 63);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(519, 81);
            this.StatusLabel.TabIndex = 3;
            this.StatusLabel.Text = "READY";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 37);
            this.label1.TabIndex = 4;
            this.label1.Text = "Status:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PortStatusLabel,
            this.DUTStatusLabel,
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 820);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1103, 25);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // PortStatusLabel
            // 
            this.PortStatusLabel.AutoSize = false;
            this.PortStatusLabel.Name = "PortStatusLabel";
            this.PortStatusLabel.Size = new System.Drawing.Size(200, 20);
            this.PortStatusLabel.Text = "PortStatusLabel";
            // 
            // DUTStatusLabel
            // 
            this.DUTStatusLabel.Name = "DUTStatusLabel";
            this.DUTStatusLabel.Size = new System.Drawing.Size(90, 20);
            this.DUTStatusLabel.Text = "DUTStatusLabel";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(98, 20);
            this.toolStripStatusLabel1.Text = "AngleStatusLabel";
            // 
            // HistoryBox
            // 
            this.HistoryBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HistoryBox.ContextMenuStrip = this.contextMenuStrip1;
            this.HistoryBox.FormattingEnabled = true;
            this.HistoryBox.Location = new System.Drawing.Point(18, 526);
            this.HistoryBox.Name = "HistoryBox";
            this.HistoryBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.HistoryBox.Size = new System.Drawing.Size(1069, 225);
            this.HistoryBox.TabIndex = 6;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(123, 26);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.copyToolStripMenuItem.Text = "&Copy log";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // StatusIconLabel
            // 
            this.StatusIconLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.StatusIconLabel.BackColor = System.Drawing.Color.Transparent;
            this.StatusIconLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatusIconLabel.Font = new System.Drawing.Font("Wingdings 2", 174.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.StatusIconLabel.ForeColor = System.Drawing.Color.Red;
            this.StatusIconLabel.Location = new System.Drawing.Point(681, 217);
            this.StatusIconLabel.Margin = new System.Windows.Forms.Padding(7, 5, 7, 5);
            this.StatusIconLabel.Name = "StatusIconLabel";
            this.StatusIconLabel.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.StatusIconLabel.Size = new System.Drawing.Size(244, 232);
            this.StatusIconLabel.TabIndex = 7;
            this.StatusIconLabel.Text = "T";
            this.StatusIconLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.StatusIconLabel.Visible = false;
            // 
            // statusStrip2
            // 
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TesterStatusLabel});
            this.statusStrip2.Location = new System.Drawing.Point(0, 798);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(1103, 22);
            this.statusStrip2.TabIndex = 8;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // TesterStatusLabel
            // 
            this.TesterStatusLabel.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TesterStatusLabel.Name = "TesterStatusLabel";
            this.TesterStatusLabel.Size = new System.Drawing.Size(126, 17);
            this.TesterStatusLabel.Text = "TesterStatusLabel";
            this.TesterStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1103, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetTesterCycleCountToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // resetTesterCycleCountToolStripMenuItem
            // 
            this.resetTesterCycleCountToolStripMenuItem.Name = "resetTesterCycleCountToolStripMenuItem";
            this.resetTesterCycleCountToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.resetTesterCycleCountToolStripMenuItem.Text = "Reset Tester Cycle Count";
            this.resetTesterCycleCountToolStripMenuItem.Click += new System.EventHandler(this.resetTesterCycleCountToolStripMenuItem_Click);
            // 
            // StatusBar
            // 
            this.StatusBar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StatusBar.BackColor = System.Drawing.Color.Red;
            this.StatusBar.Location = new System.Drawing.Point(19, 200);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(866, 363);
            this.StatusBar.TabIndex = 11;
            this.StatusBar.Visible = false;
            // 
            // ProgressBar
            // 
            this.ProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProgressBar.BarColor = System.Drawing.Color.Blue;
            this.ProgressBar.Location = new System.Drawing.Point(22, 169);
            this.ProgressBar.Maximum = 100F;
            this.ProgressBar.Minimum = 0F;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(863, 23);
            this.ProgressBar.TabIndex = 12;
            this.ProgressBar.Value = 0F;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundBrush.Angle = 0F;
            this.BackgroundBrush.Color1 = System.Drawing.Color.White;
            this.BackgroundBrush.Color2 = System.Drawing.Color.Gainsboro;
            this.BackgroundBrush.Mode = IDS.GradientBrush.GradientMode.ForwardDiagonal;
            this.BackgroundBrush.Rectangle = new System.Drawing.Rectangle(0, 0, 1103, 845);
            this.ClientSize = new System.Drawing.Size(1103, 845);
            this.Controls.Add(this.StatusIconLabel);
            this.Controls.Add(this.ProgressBar);
            this.Controls.Add(this.statusStrip2);
            this.Controls.Add(this.HistoryBox);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.UserButton);
            this.Controls.Add(this.SerialNumberLabel);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.label1);
            this.Location = new System.Drawing.Point(0, 0);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.RS232_Opened += new IDS.CommShareForm.RS232_OpenedDelegate(this.Form1_RS232_Opened);
            this.RS232_Closed += new IDS.CommShareForm.RS232_ClosedDelegate(this.Form1_RS232_Closed);
            this.RS232_Rx += new IDS.CommShareForm.RS232_RxDelegate(this.Form1_RS232_Rx);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button UserButton;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label SerialNumberLabel;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel PortStatusLabel;
        private System.Windows.Forms.ListBox HistoryBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel DUTStatusLabel;
        private System.Windows.Forms.Label StatusIconLabel;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripStatusLabel TesterStatusLabel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetTesterCycleCountToolStripMenuItem;
        private System.Windows.Forms.Panel StatusBar;
        private IDS.ProgressBar ProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}

