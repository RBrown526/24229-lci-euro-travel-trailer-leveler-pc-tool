//#define DEBUG

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using LincTabDB;

namespace _21602_LCI_myRV_TT_Leveler_SE_ICT
{
    public partial class Form1 : IDS.CommShareForm
    {
        // test control variables
        static Form1 Form = null; // the form
        
        // test parameters
        const String AssemblyNumber = "23944A";
        const String SoftwarePartNumber = "22204-B";
        const String TesterPartNumber = "21307A";
		
		const double MaxShuntCurrentRelayOff = 1.0; // amps
        const double MinShuntCurrentRelayOn = 2.0; // amps
        const double MaxShuntCurrentRelayOn = 5.0; // amps
		
		
        const int TesterBaudRate = 115200;
        const string FailIcon = "T";
        const string PassIcon = "R";
        const string BusyIcon = "";
        const double DefaultTimeout = 3.0; // seconds
        const double ProgrammingDelay = 0.5; // seconds
        const double PowerDownDelay = 0.5; // seconds
        const double StartButtonTimeout = 120; // seconds
        const double TestDelay = 0.10;
		const double RelayTestDelay = 0.20;
        DialogResult Result = DialogResult.None;
        Color background = Color.Transparent;
		
		// required tolerance when moving to a target angle
        const double TightAngleTolerance = 0.05;
        const double AngleTolerance = 0.10;

        IDS.Timer StateTime = new IDS.Timer();
        IDS.Timer MessageTime = new IDS.Timer();
        static DateTime TestTime;

        string TesterBarCode = "";
        string PassFailLogBaseFileName;
        string MasterPassLogFileName;
        string ICTCycleCountLogFileName;
		
		static int RFMessageCount = 0;

        const int ICTCycleCountMaxWarningRepeat = 100;    // after max is reached, only warning every x tests
        int ICTCycleCountMax = 20000;
        const double DefaultMessagePeriod = 0.15; // seconds (messages to tester)

        StringBuilder StringBuilder = new StringBuilder();

        class PeriodicMessage
        {
            protected byte[] Message = null;
            protected bool SendNow = true;
            IDS.Timer Timer = new IDS.Timer();
            double Rate;
            bool Pause;

            public PeriodicMessage(double rate, byte[] message)
            {
                Rate = rate;
                Message = message;
            }

            public void PauseMessage()
            {
                Pause = true;
            }

            public void UnpauseMessage()
            {
                Pause = false;
            }
            
            public bool TimeToSendMessage
            {
                get
                {
                    if (Pause) return false;
                    if (SendNow) return true;
                    return Timer.Elapsed > Rate;
                }
            }

            public byte[] OutgoingMessage
            {
                get
                {
                    SendNow = false;
                    Timer.Reset();
                    return Message;
                }
            }
        }

        class TesterObject : PeriodicMessage
        {
            public int ICTCycleCount;

            // State xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx
            //       |||||||| |||||||| |||||||| ||||||||
            //       |||||||| |||||||| |||||||| ||||||| \______ EXT_BUTTON
            //       |||||||| |||||||| |||||||| |||||| \_______ unused
            //       |||||||| |||||||| |||||||| ||||| \________ unused
            //       |||||||| |||||||| |||||||| |||| \_________ TJHall2Power
            //       |||||||| |||||||| |||||||| ||| \__________ TJHall1Power
            //       |||||||| |||||||| |||||||| || \___________ RRHallPower
            //       |||||||| |||||||| |||||||| | \____________ LRHallPower
            //       |||||||| |||||||| ||||||||  \_____________ RFHallPower
            //       |||||||| |||||||| ||||||||
            //       |||||||| |||||||| ||||||| \_______________	LFHallPower
            //       |||||||| |||||||| |||||| \________________	RearSensorVBatt
            //       |||||||| |||||||| ||||| \_________________	FrontSensorVBatt
            //       |||||||| |||||||| |||| \__________________	TJp
            //       |||||||| |||||||| ||| \___________________	TJm
            //       |||||||| |||||||| || \____________________	RRm
            //       |||||||| |||||||| | \_____________________	RRp
            //       |||||||| ||||||||  \______________________	LRm
            //       |||||||| ||||||||	             	  
            //       |||||||| ||||||| \________________________	LRp
            //       |||||||| |||||| \_________________________	RFm
            //       |||||||| ||||| \__________________________	RFp
            //       |||||||| |||| \___________________________	LFm
            //       |||||||| ||| \____________________________	LFp
            //       |||||||| || \_____________________________	TJ_HF2_CONTROL
            //       |||||||| | \______________________________	TJ_HF1_CONTROL
            //       ||||||||  \_______________________________	TX_ENABLE
            //       ||||||||	                     	  
            //       ||||||| \_________________________________	RR_HF_CONTROL
            //       |||||| \__________________________________	RF_HF_CONTROL
            //       ||||| \___________________________________	LR_HF_CONTROL
            //       |||| \____________________________________	LF_HF_CONTROL
            //       ||| \_____________________________________	REAR_SENSOR_SIGNAL_CONTROL
            //       || \______________________________________	FRONT_SENSOR_SIGNAL_CONTROL
            //       | \_______________________________________	BUZZER
            //        \________________________________________	DUT_CONTROL
            public enum FLAGS : uint
            {
                DUT_CONTROL     				= 0x80000000,
                BUZZER          				= 0x40000000,
                FRONT_SENSOR_SIGNAL_CONTROL		= 0x20000000,
                REAR_SENSOR_SIGNAL_CONTROL      = 0x10000000,
                LF_HF_CONTROL     				= 0x08000000,
                LR_HF_CONTROL     				= 0x04000000,
                RF_HF_CONTROL     				= 0x02000000,
                RR_HF_CONTROL     				= 0x01000000,
                TX_ENABLE     					= 0x00800000,
                TJ_HF1_CONTROL     				= 0x00400000,
                TJ_HF2_CONTROL      			= 0x00200000,
                LFp      						= 0x00100000,
                LFm      						= 0x00080000,
                RFp      						= 0x00040000,
                RFm      						= 0x00020000,
                LRp      						= 0x00010000,
                LRm      						= 0x00008000,
                RRp      						= 0x00004000,
                RRm      						= 0x00002000,
                TJm      						= 0x00001000,
                TJp      						= 0x00000800,
                FrontSensorVBatt      			= 0x00000400,
                RearSensorVBatt      			= 0x00000200,
                LFHallPower      				= 0x00000100,
                RFHallPower      				= 0x00000080,
                LRHallPower      				= 0x00000040,
                RRHallPower      				= 0x00000020,
                TJHall1Power      				= 0x00000010,
                TJHall2Power      				= 0x00000008,
				EXT_BUTTON						= 0x00000001,
                None = 0
            }
            public FLAGS State = 0;
            bool mStateValid = false;
            IDS.Timer StateChangeTimer = new IDS.Timer();
            IDS.Timer StateTimer = new IDS.Timer();
            public bool PassFailLogUpdated;
            public bool MasterPassLogUpdated;
            public bool Passed;

            public const FLAGS DUT_Power_Flags = TesterObject.FLAGS.DUT_CONTROL | TesterObject.FLAGS.EXT_BUTTON;
			// arh - TJHall1Power and TJHall2Power are DNP on the current board
            //public const FLAGS DUT_Hall_Power_Flags = TesterObject.FLAGS.DUT_CONTROL | TesterObject.FLAGS.EXT_BUTTON | TesterObject.FLAGS.LFHallPower | TesterObject.FLAGS.LRHallPower | TesterObject.FLAGS.RFHallPower | TesterObject.FLAGS.RRHallPower | TesterObject.FLAGS.TJHall1Power | TesterObject.FLAGS.TJHall2Power;
			public const FLAGS DUT_Hall_Power_Flags = TesterObject.FLAGS.DUT_CONTROL | TesterObject.FLAGS.EXT_BUTTON | TesterObject.FLAGS.LFHallPower | TesterObject.FLAGS.LRHallPower | TesterObject.FLAGS.RFHallPower | TesterObject.FLAGS.RRHallPower;

            public TesterObject()
                : base(0.15, new byte[] { 0x10, 0x00, 0x00})
            {
            }

            public String String
            {
                get
                {
                    String s = "";
                    if ((int)State == 0)
                        return "None";
                    foreach (FLAGS f in Enum.GetValues(typeof(FLAGS)))
                    {
                        if (f != 0 && FlagsSet(f))
                            s += f.ToString() + " ";
                    }
                    return s.Trim();
                }
            }

            public bool FlagsSet(FLAGS f)
            {
                if (!StateValid)
                    return false;
                return (State & f) == f;
            }

            public bool FlagsClear(FLAGS f)
            {
                if (!StateValid) return false;
                return (State & f) == 0;
            }

            public void ResetState()
            {
                State = 0;
				UnpauseMessage();
                mStateValid = false;
            }

            /// <summary>
            /// Sets the state received from the tester
            /// </summary>
            /// <param name="b1"></param>
            /// <param name="b2"></param>
            /// <param name="b3"></param>
            /// <param name="b4"></param>
            public void MessageIn(byte b1, byte b2, byte b3, byte b4)
            {
                uint s = (uint) ((b1 << 24) | (b2 << 16) | (b3 << 8) | b4);

                if (State != (FLAGS)s)
                    StateChangeTimer.Reset();

                State = (FLAGS)s;
				
				// don't care about TX enable - let the tester control it as needed
                State &= ~FLAGS.TX_ENABLE;
                
                mStateValid = true;
                StateTimer.Reset();
            }

            public bool StateValid
            {
                get
                {
                    if (StateTimer.Elapsed > 1)
                        mStateValid = false;
                    return mStateValid;
                }
            }

            /// <summary>
            /// Sets outputs for the tester to control
            /// </summary>
            /// <param name="dut_power"></param>
            /// <param name="buzzer"></param>
            public void SetOutputs(FLAGS flags)
            {
                byte b1 = (byte)((int)flags >> 24);
                byte b2 = (byte)((int)flags >> 16);
				
				b1 &= 0xFF;			// all flags in this byte are outputs
				b2 &= 0x60;			// mask out non-output flags (only keep 0x40 == TJ_HF1_CONTROL and 0x20 == TJ_HF2_CONTROL)
               
                if (Message[1] != b1 || Message[2] != b2)
                {
                    Message[1] = b1;
                    Message[2] = b2;
                    SendNow = true;
                }
            }
        }
        static TesterObject Tester = new TesterObject();

		class DUTOutputObject : PeriodicMessage
        {
            // State xxxxxxxx xxxxxxxx
            //       |||||||| ||||||||
            //       |||||||| ||||||| \______ unused
            //       |||||||| |||||| \_______ unused
            //       |||||||| ||||| \________ TP_TX_CONTROL
            //       |||||||| |||| \_________ HALL_EFFECT_POWER_CONTROL
            //       |||||||| ||| \__________ EXT_SENSOR_POWER_CONTROL
            //       |||||||| || \___________ COIL_CONTROL
            //       |||||||| | \____________ F_EXT_CONTROL
            //       ||||||||  \_____________ F_RET_CONTROL
            
            //       ||||||| \_______________ LR_RET_CONTROL
            //       |||||| \________________ LR_EXT_COTNROL
            //       ||||| \_________________ RR_RET_CONTROL
            //       |||| \__________________ RR_EXT_CONTROL
            //       ||| \___________________ LF_RET_CONTROL
            //       || \____________________ LF_EXT_CONTROL
            //       | \_____________________ RF_RET_CONTROL
            //        \______________________ RF_EXT_CONTROL
            
            public enum FLAGS : int
            {
                RF_EXT_CONTROL              = 0x8000,
                RF_RET_CONTROL              = 0x4000,
                LF_EXT_CONTROL 				= 0x2000,
                LF_RET_CONTROL 				= 0x1000,
                RR_EXT_CONTROL 				= 0x0800,
                RR_RET_CONTROL 				= 0x0400,
                LR_EXT_CONTROL 				= 0x0200,
                LR_RET_CONTROL 				= 0x0100,
                F_RET_CONTROL 				= 0x0080,
                F_EXT_CONTROL 				= 0x0040,
                COIL_CONTROL 				= 0x0020,
                EXT_SENSOR_POWER_CONTROL 	= 0x0010,
                HALL_EFFECT_POWER_CONTROL 	= 0x0008,
                TP_TX_CONTROL 				= 0x0004,

                None = 0
            }

            public FLAGS State = 0;
            bool mStateValid = false;
            IDS.Timer StateChangeTimer = new IDS.Timer();
            IDS.Timer StateTimer = new IDS.Timer();
            
            public DUTOutputObject()
                : base(0.20, new byte[] { 0x00, 0xF9, 0x13, 0x00, 0x00 })
            {
            }

            public String String
            {
                get
                {
                    String s = "";
                    if ((int)State == 0)
                        return "None";
                    foreach (FLAGS f in Enum.GetValues(typeof(FLAGS)))
                    {
                        if (f != 0 && FlagsSet(f))
                            s += f.ToString() + " ";
                    }
                    return s.Trim();
                }
            }

            public bool FlagsSet(FLAGS f)
            {
                if (StateValid)
                    return (State & f) == f;
                return false;
            }

            public bool FlagsClear(FLAGS f)
            {
                if (StateValid)
                    return (State & f) == 0;
                return false;
            }

            public void ResetState()
            {
                mStateValid = false;
                UnpauseMessage();
                State = 0;
            }

            /// <summary>
            /// Sets the state received from the tester
            /// </summary>
            /// <param name="b1"></param>
            /// <param name="b2"></param>
            /// <param name="b3"></param>
            /// <param name="b4"></param>
            /// <param name="b5"></param>
            /// <param name="b6"></param>
            /// <param name="b7"></param>
            public void MessageIn(byte b1, byte b2)
            {
                int s = (b1 << 8) | (b2 << 0);
                
                if (State != (FLAGS)s)
                    StateChangeTimer.Reset();
                State = (FLAGS)s;
                mStateValid = true;
                StateTimer.Reset();
            }

            public bool StateValid
            {
                get
                {
                    if (StateTimer.Elapsed > 1)
                        mStateValid = false;
                    return mStateValid;
                }
            }

            /// <summary>
            /// Sets outputs for the tester to control
            /// </summary>
            /// <param name="output"></param>
            public void SetOutputs(FLAGS output)
            {
                byte b3 = (byte)((int)output >> 8);
                byte b4 = (byte)((int)output >> 0);
                
                if ((Message[3] != b3) || (Message[4] != b4))
                {
                    Message[3] = b3;
                    Message[4] = b4;
                    SendNow = true;
                }
            }
        }
        static DUTOutputObject DUTOutputs = new DUTOutputObject();
		
        class DUTInputObject : PeriodicMessage
        {
            // State xxxxxxxx xxxxxxxx
            //       |||||||| ||||||||
            //       |||||||| ||||||| \______ unused
            //       |||||||| |||||| \_______ unused
            //       |||||||| ||||| \________ unused
            //       |||||||| |||| \_________ unused
            //       |||||||| ||| \__________ unused
            //       |||||||| || \___________ TJ_HALL_FEEDBACK_2
            //       |||||||| | \____________ TJ_HALL_FEEDBACK_1
            //       ||||||||  \_____________ REAR_SENSOR_DATA_SIGNAL
            //       ||||||||
            //       ||||||||
            //       ||||||| \_______________ FRONT_SENSOR_DATA_SIGNAL
            //       |||||| \________________ TP_DATA_RX
            //       ||||| \_________________ HALL_POWER_SPROT
            //       |||| \__________________ EXT_SENSOR_SPROT
            //       ||| \___________________ RR_HALL_FEEDBACK
            //       || \____________________ LR_HALL_FEEDBACK
            //       | \_____________________ RF_HALL_FEEDBACK
			//        \______________________ LF_HALL_FEEDBACK
			

            public enum FLAGS : int
            {
                LF_HALL_FEEDBACK  			= 0x8000,
                RF_HALL_FEEDBACK  			= 0x4000,
                LR_HALL_FEEDBACK  			= 0x2000,
                RR_HALL_FEEDBACK  			= 0x1000,
                EXT_SENSOR_SPROT  			= 0x0800,
                HALL_POWER_SPROT  			= 0x0400,
                TP_DATA_RX 					= 0x0200,
                FRONT_SENSOR_DATA_SIGNAL 	= 0x0100,
                REAR_SENSOR_DATA_SIGNAL 	= 0x0080,
                TJ_HALL_FEEDBACK_1 			= 0x0040,
                TJ_HALL_FEEDBACK_2 			= 0x0020,
                None = 0
            }

            public FLAGS State = 0;
            bool mStateValid = false;
            IDS.Timer StateChangeTimer = new IDS.Timer();
            IDS.Timer StateTimer = new IDS.Timer();
            public byte[] MACaddr = new byte[] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
            public String SerialNumber = "";
            public String SoftwarePartNumber = "";

            public byte[] TstrCANTxMsg;
            public bool CanMsgTxRx = false;
			
			public double VBatt = 0;
            public double LFShunt = 0;
            public double RFShunt = 0;
            public double LRShunt = 0;
            public double RRShunt = 0;
            public double FShunt = 0;

            public DUTInputObject()
                : base(0.150, new byte[] { 0x00, 0xF9, 0x10})
            {
            }

            public String String
            {
                get
                {
                    String s = "";
                    if ((int)State == 0)
                        return "None";
                    foreach (FLAGS f in Enum.GetValues(typeof(FLAGS)))
                    {
                        if (f != 0 && FlagsSet(f))
                            s += f.ToString() + " ";
                    }
                    return s.Trim();
                }
            }

            public bool FlagsSet(FLAGS f)
            {
                if (StateValid)
                    return (State & f) == f;
                return false;
            }

            public bool FlagsClear(FLAGS f)
            {
                if (StateValid)
                    return (State & f) == 0;
                return false;
            }

            public void ResetState()
            {
                mStateValid = false;
                State = 0;
				VBatt = 0;
                LFShunt = 0;
                RFShunt = 0;
                LRShunt = 0;
                RRShunt = 0;
				FShunt = 0;
				UnpauseMessage();
                
                CanMsgTxRx = false;
            }

            /// <summary>
            /// Sets the state received from the tester
            /// </summary>
            /// <param name="b1"></param>
            /// <param name="b2"></param>
            /// <param name="b3"></param>
            /// <param name="b4"></param>
            /// <param name="b5"></param>
            /// <param name="b6"></param>
            /// <param name="b7"></param>
            /// <param name="b8"></param>
            public void MessageIn(byte b1, byte b2, byte b3, byte b4, byte b5, byte b6, byte b7, byte b8)
            {
                int s = ((b1 << 8) | b2);
				
                if (State != (FLAGS)s)
                    StateChangeTimer.Reset();
                State = (FLAGS)s;
				
				// strip out EXT_SENSOR_SPROT
				State &= ~FLAGS.EXT_SENSOR_SPROT;
                
				// strip out TP_DATA_RX
				State &= ~FLAGS.TP_DATA_RX;
				
                mStateValid = true;
                StateTimer.Reset();

                double vref = 3.3;
                double adc_max = 255;
                double shunt_ohms = 0.001;
                double gain_ohms = 4530;

                LFShunt = (b3 * vref) / (adc_max * 0.01 * gain_ohms * shunt_ohms);
                RFShunt = (b4 * vref) / (adc_max * 0.01 * gain_ohms * shunt_ohms);
                RRShunt = (b5 * vref) / (adc_max * 0.01 * gain_ohms * shunt_ohms);
                LRShunt = (b6 * vref) / (adc_max * 0.01 * gain_ohms * shunt_ohms);
                FShunt = (b7 * vref) / (adc_max * 0.01 * gain_ohms * shunt_ohms);
  
                VBatt = b8 * vref / adc_max * (221 + 47) / 47;
            }

            public bool StateValid
            {
                get
                {
                    if (StateTimer.Elapsed > 1)
                        mStateValid = false;
                    return mStateValid;
                }
            }

            /// <summary>
            /// Sets outputs for the tester to control
            /// </summary>
            /// <param name="output"></param>
            public void SetOutputs(FLAGS output)
            {
                SendNow = true;
            }
        }
        static DUTInputObject DUTInputs = new DUTInputObject();

        public Form1()
        {
            Form = this;
            
            InitializeComponent();
#if !DEBUG
//In release mode, we write to the database on the network, otherwise the test db is used
            LincTabDB.LincTabDB.Production_DB = true;
#endif
            Text = IDS.AppInfo.Title;

            PassFailLogBaseFileName = IDS.AppInfo.ProductName;
            MasterPassLogFileName = IDS.AppInfo.ProductName + " Master Pass Log.txt";
            ICTCycleCountLogFileName = IDS.AppInfo.ProductName + " Cycle Count Log.txt";

            // add about box to system menu
            IDS.Form.SysMenu menu = this.SystemMenu;
            menu.AppendSeparator();
            menu.AppendMenu("About...", new SysMenu.Handler(delegate { new IDS.AboutBox().ShowDialog(this); }));

            // create interface objects to handle each state
            Array states = Enum.GetValues(typeof(STATE));
            foreach (STATE state in states)
            {
                Type t = (Type)IDS.EnumAttribute.Get(state, typeof(StateHandler));
                if (t != null)
                    Dictionary.Add(state, (StateInterface)Activator.CreateInstance(t));
            }

            // initialize status bar
            PortStatusLabel.Text = "";

            // initialize bar-code reader to scan for assembly barcode label
            IDS.BarCodeReader.Add(AssemblyNumber +   "dddddddddd", this.OnAssemblyBarCodeScanned);
            IDS.BarCodeReader.Add(TesterPartNumber + "dddddddddd", true, this.OnTesterBarCodeScanned);

            int xcenter = HistoryBox.Location.X + (HistoryBox.Width / 2);

            TesterBarCode = "";
            StatusIconLabel.Text = "";
            StatusIconLabel.Location = new Point(xcenter - (StatusIconLabel.Width / 2), StatusIconLabel.Location.Y);
            StatusIconLabel.Visible = false;
            StatusLabel.Width = HistoryBox.Width;
            StatusBar.Visible = false;
            StatusBar.Location = new Point(xcenter - (StatusBar.Width / 2), StatusBar.Location.Y); 
            StatusBar.Width = HistoryBox.Width;
            StatusBar.Location = new Point(HistoryBox.Location.X, StatusBar.Location.Y);

            ProgressBar.Location = new Point(HistoryBox.Location.X, ProgressBar.Location.Y);
            ProgressBar.Width = HistoryBox.Width;

            Form1_RS232_Closed(); 
        }

        // attribute used to specify a handler class for each state
        class StateHandler : IDS.EnumAttribute { public StateHandler(Type n) : base(n) { } }

        // dictionary that can lookup a state interface object for a particular enum
        Dictionary<STATE, StateInterface> Dictionary = new Dictionary<STATE, StateInterface>();

        // every state requires an interface
        interface StateInterface
        {
            void OnStateEnter();
            void Task();
            void OnRX(RxMessage message);
        }

        //matt
        enum STATE
        {	
            [StateHandler(typeof(State_ComPortClosed))]
            ComPortClosed,
            [StateHandler(typeof(State_GetTesterBarCode))]
            GetTesterBarCode,
            [StateHandler(typeof(State_Ready))]
            Ready,
            [StateHandler(typeof(State_Start))]
            Start,
            [StateHandler(typeof(State_CheckICTCycleCount))]
            CheckICTCycleCount,
            [StateHandler(typeof(State_CheckAssemblyInfo))]
            CheckAssemblyInfo,
            [StateHandler(typeof(State_CheckAssemblyInfo))]
            CheckAssemblyInfoWait,
            [StateHandler(typeof(State_RemoveAssemblyFromLogFile))]
            RemoveAssemblyFromLogFile,
            [StateHandler(typeof(State_GetDataFromDatabase))]
            GetDataFromDatabase,
            [StateHandler(typeof(State_GetDataFromDatabaseWait))]
			GetDataFromDatabaseWait,
            [StateHandler(typeof(State_WaitForStartButton))]
			WaitForStartButton,
			[StateHandler(typeof(State_IncrementICTCycleCount))]
            IncrementICTCycleCount,
            [StateHandler(typeof(State_StartTest))]
			StartTest,
			
			[StateHandler(typeof(State_TurnOnTesterMessages))]
            TurnOnTesterMessages1,
			[StateHandler(typeof(State_PowerUp))]
            PowerUp1,
            [StateHandler(typeof(State_ReadPartNumber))]
            ReadPartNumber,
            [StateHandler(typeof(State_SetProductionByte))]
            SetProductionByte,

            [StateHandler(typeof(State_WriteMac))]
            WriteMac,
            [StateHandler(typeof(State_CheckCAN))]
            CheckCAN,
			
			[StateHandler(typeof(State_CheckDefaults))]
            CheckDefaults,

            [StateHandler(typeof(State_CheckExtSensorVBATTOn))]
            CheckExtSensorVBATTOn,
            [StateHandler(typeof(State_CheckFrontSensorDataOn))]
            CheckFrontSensorDataOn, 
			[StateHandler(typeof(State_CheckRearSensorDataOn))]
            CheckRearSensorDataOn,

            /*
            [StateHandler(typeof(State_TurnOffTesterMessages))]
            TurnOffTesterMessages1,
            [StateHandler(typeof(State_TurnOnRFDebug))]
            TurnOnRFDebug,
            [StateHandler(typeof(State_TestRF))]
            TestRF,
            [StateHandler(typeof(State_TurnOffRFDebug))]
            TurnOffRFDebug,
            [StateHandler(typeof(State_TurnOnTesterMessages))]
            TurnOnTesterMessages2,
            */

            [StateHandler(typeof(State_CheckHallEffectPower))]
            CheckHallEffectPower,
            [StateHandler(typeof(State_CheckHallEffectSignalsHI))]
            CheckHallEffectSignalsHI,
            [StateHandler(typeof(State_CheckLFHF))]
            CheckLFHF,
            [StateHandler(typeof(State_CheckLRHF))]
            CheckLRHF,
            [StateHandler(typeof(State_CheckRFHF))]
            CheckRFHF,
            [StateHandler(typeof(State_CheckRRHF))]
            CheckRRHF,
			[StateHandler(typeof(State_CheckTJHF1))]
            CheckTJHF1,
            [StateHandler(typeof(State_CheckTJHF2))]
            CheckTJHF2,
			
            [StateHandler(typeof(State_TestLFMotorExt))]
            TestLFMotorExt,
            [StateHandler(typeof(State_TestLFMotorRet))]
            TestLFMotorRet,
            [StateHandler(typeof(State_TestRFMotorExt))]
            TestRFMotorExt,
            [StateHandler(typeof(State_TestRFMotorRet))]
            TestRFMotorRet,
            [StateHandler(typeof(State_TestLRMotorExt))]
            TestLRMotorExt,
            [StateHandler(typeof(State_TestLRMotorRet))]
            TestLRMotorRet,
            [StateHandler(typeof(State_TestRRMotorExt))]
            TestRRMotorExt,
            [StateHandler(typeof(State_TestRRMotorRet))]
            TestRRMotorRet,
			[StateHandler(typeof(State_TestFMotorExt))]
            TestFMotorExt,
            [StateHandler(typeof(State_TestFMotorRet))]
            TestFMotorRet,
			[StateHandler(typeof(State_TurnOffMotors))]
            TurnOffMotors,
			
            // set shipping zero points
            [StateHandler(typeof(State_WriteFrontXZero))]
            WriteFrontXZero,
            [StateHandler(typeof(State_WaitForWrite))]
            WriteDelay2,
            [StateHandler(typeof(State_WriteFrontYZero))]
            WriteFrontYZero,
            [StateHandler(typeof(State_WaitForWrite))]
            WriteDelay3,
            [StateHandler(typeof(State_WriteRearXZero))]
            WriteRearXZero,
            [StateHandler(typeof(State_WaitForWrite))]
            WriteDelay4,
            [StateHandler(typeof(State_WriteRearYZero))]
            WriteRearYZero,
            [StateHandler(typeof(State_WaitForWrite))]
            WriteDelay5,
            [StateHandler(typeof(State_WriteHookupAngle))]
            WriteHookupAngle,
            [StateHandler(typeof(State_WaitForWrite))]
            WriteDelay6,
            [StateHandler(typeof(State_WriteZeroValid))]
            WriteZeroValid,
            [StateHandler(typeof(State_WaitForWrite))]
            WriteDelay7,
            [StateHandler(typeof(State_SetJackShippingState))]
            SetJackShippingState,
            [StateHandler(typeof(State_WaitForWrite))]
            WriteDelay8,
            [StateHandler(typeof(State_ClearProductionByte))]
            ClearProductionByte,
            [StateHandler(typeof(State_WaitForWrite))]
            WriteDelay9,
  
            [StateHandler(typeof(State_PowerDown))]
            PowerDown1,
            [StateHandler(typeof(State_PowerDownDelay))]
            PowerDownDelay,
            [StateHandler(typeof(State_PowerUp))]
            PowerUp2,
            
            // verify shipping zero points
            [StateHandler(typeof(State_VerifyFrontXZero))]
            VerifyFrontXZero,
            [StateHandler(typeof(State_VerifyFrontYZero))]
            VerifyFrontYZero,
            [StateHandler(typeof(State_VerifyRearXZero))]
            VerifyRearXZero,
            [StateHandler(typeof(State_VerifyRearYZero))]
            VerifyRearYZero,
            [StateHandler(typeof(State_VerifyHookupAngle))]
            VerifyHookupAngle,
            [StateHandler(typeof(State_VerifyZeroValid))]
            VerifyZeroValid,
            [StateHandler(typeof(State_CheckProductionByte))]
            CheckProductionByte,
			
            [StateHandler(typeof(State_Pass))]
            Pass,
            [StateHandler(typeof(State_EndTest))]
            EndTest,
            [StateHandler(typeof(State_UpdateLogs))]
            UpdateLogs,
            [StateHandler(typeof(State_ShowResults))]
            ShowResults
        }

        static STATE mState = STATE.ComPortClosed;
        static STATE State
        {
            get { return mState; }

            set
            {
                mState = value;

                Form.StateTime.Reset();
                Tester.ResetState();

                // assume the status background is transparent
                Form.background = Color.Transparent;

                // handle progress bar
                if (value == STATE.ShowResults)
                {
                    if (Tester.Passed)
                    {
                        Form.ProgressBar.Value = Form.ProgressBar.Maximum;
                        Form.ProgressBar.Text = "100%%";
                        Form.ProgressBar.BarColor = Color.Lime;
                    }
                    else
                        Form.ProgressBar.BarColor = Color.Red;
                }
                else if (value >= STATE.CheckICTCycleCount && value <= STATE.IncrementICTCycleCount)
                {
                    Form.ProgressBar.Value = Form.ProgressBar.Minimum;
                    Form.ProgressBar.Text = "";
                    Form.ProgressBar.BarColor = Color.Gold;
                }
                else if (value >= STATE.StartTest && value < STATE.Pass)
                {
                    int min = (int)STATE.StartTest;
                    int max = (int)STATE.Pass;
                    int val = (int)value;
                    float percent = (float)(val - min) / (max - min);
                    if (percent <= Form.ProgressBar.Maximum)
                    {
                        Form.ProgressBar.Value = Form.ProgressBar.Maximum * percent;
                        int p = (int)(100 * percent);
                        Form.ProgressBar.Text = p.ToString() + "%%";
                    }
                    Form.ProgressBar.BarColor = Color.Gold;
                }

                Form.StatusLabel.BackColor = Form.background;

                // kick start the state
                StateInterface i;
                if (Form.Dictionary.TryGetValue(State, out i))
                    i.OnStateEnter();
            }
        }

        String History
        {
            set
            {
                StatusLabel.Text = value;
                HistoryBox.Items.Add(value);

                // force item to become visible
                int itemsPerPage = (int)(HistoryBox.Height / HistoryBox.ItemHeight);
                HistoryBox.TopIndex = HistoryBox.Items.Count - itemsPerPage;
            }
        }


// matt
///////////////////////////////////
///////////////////////////////////
///////////////////////////////////
///////////////////////////////////

        class State_ComPortClosed : StateInterface
        {
            public void OnStateEnter()
            {
                Form.StatusLabel.Text = "Open COM port (115200 baud)";
            }

            public void Task()
            {
                Tester.SetOutputs(TesterObject.FLAGS.None);
                DUTInputs.SetOutputs(DUTInputObject.FLAGS.None);
            }

            public void OnRX(RxMessage message)
            {
            }
        }

        class State_GetTesterBarCode : StateInterface
        {
            public void OnStateEnter()
            {
                Form.StatusLabel.BackColor = Color.Transparent;
                Form.StatusLabel.ForeColor = Color.Black;
                Form.StatusLabel.Text = "Scan Tester's bar code";
            }

            public void Task()
            {
                if (Form.TesterBarCode != "")
                    State = State + 1;
            }

            public void OnRX(RxMessage message)
            {
            }
        }

        class State_Ready : StateInterface
        {
            public void OnStateEnter()
            {
                Form.SerialNumberLabel.Text = "Serial number:";
                Form.StatusLabel.Text = "Scan assembly serial number bar code to begin";
            }

            public void Task()
            {
                DUTInputs.SetOutputs(DUTInputObject.FLAGS.None);
            }

            public void OnRX(RxMessage message)
            {
            }
        }
        
        class State_Start : StateInterface
        {
            public void OnStateEnter()
            {
                Form.HistoryBox.Items.Clear();
                TestTime = DateTime.Now;
                Form.History = "Test started at " + TestTime.ToLocalTime();
                Form.SerialNumberLabel.Text = "Serial number: " + DUTInputs.SerialNumber;
                Form.History = Form.SerialNumberLabel.Text;
                State = State + 1;
            }

            public void Task()
            {
                State = State + 1;
            }

            public void OnRX(RxMessage message)
            {
            }
        }
        
        class State_CheckICTCycleCount : StateInterface
        {
            public void OnStateEnter()
            {
                Form.StatusLabel.Text = "";
                Form.StatusLabel.BackColor = Color.Transparent;
                Form.StatusLabel.ForeColor = Color.Black;
                Form.StatusIconLabel.Visible = false;
                Form.StatusIconLabel.ForeColor = Color.Transparent;
                Form.StatusIconLabel.Text = BusyIcon;
                Form.StatusIconLabel.SendToBack();
                Form.StatusIconLabel.Visible = true;
                Form.StatusBar.Visible = false;
                Form.StatusBar.ForeColor = Color.Black;
                Form.StatusBar.Text = BusyIcon;
                Tester.Passed = false;
            }

            public void Task()
            {
                Tester.ICTCycleCount = Form.GetICTCycleCount();
                if (Tester.ICTCycleCount >= Form.ICTCycleCountMax)
                {
                    Form.ICTCycleCountMax = Tester.ICTCycleCount;
                    Form.StatusLabel.Text = "This tester's cycle count is " + Tester.ICTCycleCount.ToString("0,000") + " which exceeds the maximum cycles.  Notify the job leader that tester maintenance needs to be performed.  Disposable parts such as pogo pins and harnesses need to be replaced.";
                    Form.ShowUserButton("OK");
                }
                else if (Tester.ICTCycleCount >= 0)
                {
                    State++;
                }
                else if (Form.StateTime.Elapsed >= 5.0)
                {
                    Form.StatusLabel.Text = "Problem accessing ICT cycle count log.  Please inform job leader if this warning continues to occur.";
                    Form.ShowUserButton("OK");
                }
            }

            public void OnRX(RxMessage message)
            {
            }
        }
        
        class State_CheckAssemblyInfo : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Checking master log file for duplicate item";
            }

            public void Task()
            {
                if (Form.CheckMasterLogFile(DUTInputs.SerialNumber))
                {
                    //item in master log, prompt user
                    State = State + 1;
                    string caption = "WARNING";
                    string msgText = "Please inform your supervisor of the following issue:\n ";
                    msgText += "This serial number has already passed test.\n ";
                    msgText += "Do you want to continue with the test?";
                    Form.BeginInvoke(((MethodInvoker)delegate { Form.NoticeDialogYesNo(caption, msgText, STATE.RemoveAssemblyFromLogFile, STATE.Ready); }));

                }
                else
                {
                    //item not in master log, continue
                    State = STATE.GetDataFromDatabase;
                }
            }

            public void OnRX(RxMessage message)
            {
            }
        }
        
        class State_CheckAssemblyInfoWait : StateInterface
        {
            public void OnStateEnter()
            {
            }

            public void Task()
            {
                // nothing to do
            }

            public void OnRX(RxMessage message)
            {
            }
        }
        
        class State_RemoveAssemblyFromLogFile : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Removing Assembly from master log file";
            }

            public void Task()
            {
                if (!Tester.MasterPassLogUpdated)
                {
                    //remove assembly from log file
                    string log = DUTInputs.SerialNumber;
                    Tester.MasterPassLogUpdated = Form.UpdateMasterLogFile(log, false);
                }

                if (Tester.MasterPassLogUpdated)
                    State = State + 1;
                else if (Form.StateTime.Elapsed >= 10.0)
                {
                    Tester.Passed = false;
                    string f = "";
                    if (!Tester.MasterPassLogUpdated)
                        f = "Error updating master pass log. ";
                    Form.Fail(f);
                }
                
            }

            public void OnRX(RxMessage message)
            {
            }
        }
        
        class State_GetDataFromDatabase : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Getting MAC from Database";
                Form.BeginInvoke(((MethodInvoker)delegate { Form.GetDataFromDataBase(); }));
                State = State + 1;
            }

            public void Task()
            {
            }

            public void OnRX(RxMessage message)
            {
            }
        }
        
        class State_GetDataFromDatabaseWait : StateInterface
        {
            public void OnStateEnter()
            {
            }

            public void Task()
            {
                // nothing to do
            }

            public void OnRX(RxMessage message)
            {
            }
        }
        
        class State_WaitForStartButton : StateInterface
        {
            public void OnStateEnter()
            {
                Form.ShowUserButton("Start");
                Form.StatusIconLabel.Text = "";
                Form.StatusLabel.Text = "Press start button";
            }

            public void Task()
            {
                Tester.SetOutputs(TesterObject.FLAGS.None);
                DUTInputs.SetOutputs(DUTInputObject.FLAGS.None);
                {
                    double timeleft = StartButtonTimeout - Form.StateTime.Elapsed;
                    if (timeleft < 0)
                    {
                        State = STATE.Ready;
                        Form.HideUserButton();
                    }
                    else
                        Form.UserButton.Text = "START " + timeleft.ToString("0");
                }
				
				if(Tester.FlagsClear(TesterObject.FLAGS.EXT_BUTTON))   // EXT_BUTTON is active low
				{
					Form.HideUserButton();
					State = STATE.IncrementICTCycleCount;
				}
            }

            public void OnRX(RxMessage message)
            {
            }
        }
		
		class State_IncrementICTCycleCount : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Incrementing Cycle Count";
            }

            public void Task()
            {
                if (Form.IncrementICTCycleCount() > 0)
                    State = State + 1;
                else if (Form.StateTime.Elapsed >= 5.0)
                {
                    Form.StatusLabel.Text = "Problem accessing ICT cycle count log.  Please inform job leader if this warning continues to occur.";
                    Form.ShowUserButton("OK");
                }
            }

            public void OnRX(RxMessage message)
            {
            }
        }
        
        class State_StartTest : StateInterface
        {
            public void OnStateEnter()
            {
            }

            public void Task()
            {
                State = State + 1;
            }

            public void OnRX(RxMessage message)
            {
            }
        }
		
		class State_TurnOnTesterMessages : State_TesterMessage
        {
            static byte[] tx = { 0x61, 0x00 };
            static byte[] rx = { 0x61, 0x00 };

            public State_TurnOnTesterMessages()
                : base("Enabling Tester SCI Tx", tx, rx) { }
        }
        
		class State_PowerUp : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Powering up DUT";
            }

            public void Task()
            {
                Tester.SetOutputs(TesterObject.FLAGS.DUT_CONTROL);
                DUTOutputs.SetOutputs(DUTOutputObject.FLAGS.None);

                if (Tester.FlagsSet(TesterObject.FLAGS.DUT_CONTROL))
                    State = State + 1;
                else if (Form.StateTime.Elapsed > DefaultTimeout)
                    Form.Fail("Timeout");
            }

            public void OnRX(RxMessage message)
            {
            }
        }
		
		class State_DUT_Message : StateInterface
        {
            byte[] Tx;
            byte[] Rx;
            String str;
            
            public State_DUT_Message(String s, byte[] tx, byte[] rx)
            {
                CopyMessage(tx, rx);
                str = s;
            }

            public void CopyMessage(byte[] tx, byte[] rx)
            {
                Tx = Copy(tx);
                Rx = Copy(rx);
            }

            byte[] Copy(byte[] msg)
            {
                byte[] m = new byte[msg.Length + 1];
                for (int n = 0; n < msg.Length; n++)
                    m[n + 1] = msg[n];
                return m;
            }

            virtual public void OnStateEnter()
            {
                Form.History = str;
            }

            virtual public void Task()
            {
                if (Form.StateTime.Elapsed > 2.5)
                {
                    Form.Fail();
                }
                else if (Form.MessageTime.Elapsed > 0.1)
                {
                    Form.SendMessage(Tx);
                }
            }

            virtual public void OnRX(RxMessage message)
            {
                if (message.Length == Rx.Length && message.Data[0] == 0)
                {
                    for (int n = 1; n < Rx.Length; n++)
                    {
                        if (message.Data[n] != Rx[n])
                        {
                            String s = " ";
                            for (int m = 1; m < Rx.Length; m++)
                            {
                                s += " $";
                                s += message.Data[m].ToString("X2");
                            }
                            break;
                        }
                    }

                    State = State + 1;
                }
            }
        }
		
		class State_TesterMessage : StateInterface
        {
            String Message;
            byte[] Tx;
            byte[] Rx;

            public State_TesterMessage(String message, byte[] tx, byte[] rx)
            {
                Message = message;
                Tx = tx;
                Rx = rx;
            }

            public void OnStateEnter()
            {
                if (Message != null)
                    Form.History = Message;
            }
			
            public void Task()
            {
                if (Form.StateTime.Elapsed > 0.5)
                    Form.Fail("Loss of communication");
                else if (Form.MessageTime.Elapsed > 0.1)
                    Form.RS232_Send(Tx);
            }
			
            public void OnRX(RxMessage message)
            {
                if (message.Length == Rx.Length)
                {
                    for (int n = 0; n < Rx.Length; n++)
                    {
                        if (message.Data[n] != Rx[n])
                            return;
                    }
                    State = State + 1;
                }
            }
        }
		
		class State_ReadPartNumber : State_DUT_Message
        {
            static byte[] tx = { 0xFA };
            static byte[] rx = { 0xFA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            
            public State_ReadPartNumber()
                : base("Reading SW part number", tx, rx) { }
            
            override public void OnRX(RxMessage message)
            {
                if ((message.Length == 9) && (message.Data[0] == 0x00) && (message.Data[1] == 0xFA))
                {
                    String s = "";

                    for (int i = 0; i < SoftwarePartNumber.Length; i++)
                        s += (char)message.Data[i + 2];
					
					Form.History = "Received SW part number: " + s;

                    if (s == SoftwarePartNumber)
                        State = State + 1;
                }
            }
        }

        class State_SetProductionByte : State_DUT_Message
        {
            static byte[] tx = { 0xF9, 0x00, 0xFF };
            static byte[] rx = { 0xF9, 0x00, 0xFF };

            public State_SetProductionByte()
                : base("Setting production byte to 0xFF", tx, rx) { }
        }
		
        class State_WriteMac : State_DUT_Message
        {
            //send as big endian
            //07 40 <MAC0> <MAC1> <MAC2> <MAC3> <MAC4> <MAC5>
            static byte[] tx = { 0x40, DUTInputs.MACaddr[0], DUTInputs.MACaddr[1], DUTInputs.MACaddr[2], DUTInputs.MACaddr[3], DUTInputs.MACaddr[4], DUTInputs.MACaddr[5] }; 
            static String s = "Writing MAC," + " MAC = " + BitConverter.ToString(DUTInputs.MACaddr);
            
            public State_WriteMac() : base(s, tx, tx) { }

            public override void OnStateEnter()
            {
                for (int i=1; i<tx.Length; i++)
                    tx[i] = DUTInputs.MACaddr[i-1];

                s = "Writing MAC," + " MAC = " + BitConverter.ToString(DUTInputs.MACaddr);
                Form.History = s; 
                base.CopyMessage(tx, tx);
            }
        }
        
        class State_CheckCAN : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Checking CAN communication...";
            }

            public void Task()
            {
                DUTInputs.SetOutputs(DUTInputObject.FLAGS.None);
                
                //69 <id3> <id2> <id1> <id0> <dlc> <D0> <D1> <D2> <D3> <D4> <D5> <D6> <D7>
                DUTInputs.TstrCANTxMsg = new byte[13];
                byte[] tstmsg = new byte[1 + DUTInputs.TstrCANTxMsg.Length];
                tstmsg[0] = 0x69;
                
                //CAN id 0x10, DLC 8, data 0,1,2,3,4,5,6,7
                //The message id is fixed at 0x1C0300FF, the DUT will only echo CAN with this message Id.
                DUTInputs.TstrCANTxMsg[0] = 0x1C;
                DUTInputs.TstrCANTxMsg[1] = 0x03;
                DUTInputs.TstrCANTxMsg[2] = 0x00;
                DUTInputs.TstrCANTxMsg[3] = 0xFF;
                DUTInputs.TstrCANTxMsg[4] = 0x08;
                
                for (byte i = 0; i < 8; i++)
                {
                    DUTInputs.TstrCANTxMsg[i + 5] = i;
                }
                
                DUTInputs.TstrCANTxMsg.CopyTo(tstmsg, 1);
                
                Form.SendMessage(tstmsg);
                
                if (DUTInputs.StateValid && Tester.StateValid)
                {
                    if (Tester.State == (TesterObject.DUT_Power_Flags)
                        && DUTInputs.CanMsgTxRx == true)
                    {
                        State = State + 1;
                    }
                    else if (Form.StateTime.Elapsed > DefaultTimeout)
                        Form.Fail();
                }
                else if (Form.StateTime.Elapsed > DefaultTimeout)
                    Form.Fail("Timeout");
            }

            public void OnRX(RxMessage message)
            {
                // 0    1     2     3     4     5    6     7   8    9    10   11   12   13
                //70  <id3> <id2> <id1> <id0> <dlc> <D0> <D1> <D2> <D3> <D4> <D5> <D6> <D7>

                //see if we got MAC address back in CAN message
                //Clear extended id bit also
                if (((message.Data[1] & ~0x80) == 0x1C) && (message.Data[2] == 0x03) 
                    && (message.Data[3] == 0x00) && (message.Data[4] == 0xFF) 
                    && (message.Data[6] == DUTInputs.MACaddr[0]) 
                    && (message.Data[7] == DUTInputs.MACaddr[1]) 
                    && (message.Data[8] == DUTInputs.MACaddr[2]) 
                    && (message.Data[9] == DUTInputs.MACaddr[3]) 
                    && (message.Data[10] == DUTInputs.MACaddr[4]) 
                    && (message.Data[11] == DUTInputs.MACaddr[5]))
                {
                    DUTInputs.CanMsgTxRx = true;
                    State = State + 1;
                }
            }
        }
        
		class State_CheckDefaults : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Checking default state";
            }

            public void Task()
            {
                Tester.SetOutputs(TesterObject.FLAGS.DUT_CONTROL);
                DUTOutputs.SetOutputs(DUTOutputObject.FLAGS.None);

                if (Form.StateTime.Elapsed < TestDelay)
                    return;
                if (DUTOutputs.StateValid && Tester.StateValid)
                {
                    if (Tester.State == TesterObject.DUT_Power_Flags
                        && DUTOutputs.State == DUTOutputObject.FLAGS.None
                        && DUTInputs.State == (DUTInputObject.FLAGS.HALL_POWER_SPROT | DUTInputObject.FLAGS.TJ_HALL_FEEDBACK_1 | DUTInputObject.FLAGS.TJ_HALL_FEEDBACK_2)
                        && DUTInputs.VBatt >= 10 && DUTInputs.VBatt <= 15
                        && DUTInputs.LFShunt < MaxShuntCurrentRelayOff
                        && DUTInputs.RFShunt < MaxShuntCurrentRelayOff
                        && DUTInputs.LRShunt < MaxShuntCurrentRelayOff
                        && DUTInputs.RRShunt < MaxShuntCurrentRelayOff
                        && DUTInputs.FShunt < MaxShuntCurrentRelayOff)
                        State = State + 1;
                    else if (Form.StateTime.Elapsed > DefaultTimeout)
                        Form.Fail();
                }
                else if (Form.StateTime.Elapsed > DefaultTimeout)
                    Form.Fail("Timeout");
            }

            public void OnRX(RxMessage message)
            {
            }
        }

        class State_CheckExtSensorVBATTOn : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Checking Ext Sensor VBATT On (12V)";
            }

            public void Task()
            {
                Tester.SetOutputs(TesterObject.FLAGS.DUT_CONTROL);
                DUTOutputs.SetOutputs(DUTOutputObject.FLAGS.EXT_SENSOR_POWER_CONTROL);

                if (Tester.StateValid && (Tester.State == (TesterObject.FLAGS.FrontSensorVBatt | TesterObject.FLAGS.RearSensorVBatt | TesterObject.DUT_Power_Flags)))
                    State = State + 1;
                else if (Form.StateTime.Elapsed > DefaultTimeout)
                    Form.Fail("Timeout");
            }

            public void OnRX(RxMessage message)
            {
            }
        }

        class State_CheckFrontSensorDataOn : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Checking Front Sensor Data On (0V)";
            }

            public void Task()
            {
                Tester.SetOutputs(TesterObject.FLAGS.DUT_CONTROL | TesterObject.FLAGS.FRONT_SENSOR_SIGNAL_CONTROL);
                DUTOutputs.SetOutputs(DUTOutputObject.FLAGS.None);

                if (DUTInputs.StateValid && (DUTInputs.State == (DUTInputObject.FLAGS.FRONT_SENSOR_DATA_SIGNAL | DUTInputObject.FLAGS.HALL_POWER_SPROT | DUTInputObject.FLAGS.TJ_HALL_FEEDBACK_1 | DUTInputObject.FLAGS.TJ_HALL_FEEDBACK_2)))
                    State = State + 1;
                else if (Form.StateTime.Elapsed > DefaultTimeout)
                    Form.Fail("Timeout");
            }

            public void OnRX(RxMessage message)
            {
            }
        }
		
        class State_CheckRearSensorDataOn : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Checking Rear Sensor Data On (0V)";
            }

            public void Task()
            {
                Tester.SetOutputs(TesterObject.FLAGS.DUT_CONTROL | TesterObject.FLAGS.REAR_SENSOR_SIGNAL_CONTROL);
                DUTOutputs.SetOutputs(DUTOutputObject.FLAGS.None);

                if (DUTInputs.StateValid && (DUTInputs.State == (DUTInputObject.FLAGS.REAR_SENSOR_DATA_SIGNAL | DUTInputObject.FLAGS.HALL_POWER_SPROT | DUTInputObject.FLAGS.TJ_HALL_FEEDBACK_1 | DUTInputObject.FLAGS.TJ_HALL_FEEDBACK_2)))
                    State = State + 1;
                else if (Form.StateTime.Elapsed > DefaultTimeout)
                    Form.Fail("Timeout");
            }

            public void OnRX(RxMessage message)
            {
            }
        }

        class State_TurnOffTesterMessages : State_TesterMessage
        {
            static byte[] tx = { 0x61, 0x01 };
            static byte[] rx = { 0x61, 0x01 };

            public State_TurnOffTesterMessages()
                : base("Disabling Tester SCI Tx", tx, rx) { }
        }

        class State_TurnOnRFDebug : State_DUT_Message
        {
            static byte[] tx = { 0x02, 0x04 };
            static byte[] rx = { 0x02, 0x04 };
            
            public State_TurnOnRFDebug()
                : base("Enabling RF debug", tx, rx) { }
        }
		
		class State_TestRF : StateInterface
        {
            IDS.Timer TxMessageTimer = new IDS.Timer();
            IDS.Timer RFMessageTimer = new IDS.Timer();
            bool FirstMessageRx = false;
            byte [] TxMessage = { 0x11, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55 };

            public void OnStateEnter()
            {
                Form.History = "Checking Wireless RF";
                FirstMessageRx = false;
                RFMessageCount = 0;
                DUTInputs.PauseMessage();
                DUTOutputs.PauseMessage();
                Tester.PauseMessage();
            }

            public void Task()
            {
                Tester.SetOutputs(TesterObject.FLAGS.DUT_CONTROL);
                DUTOutputs.SetOutputs(DUTOutputObject.FLAGS.None);
                String FailString;

                if (Form.StateTime.Elapsed > 10)
				{
					DUTInputs.UnpauseMessage();
                    DUTOutputs.UnpauseMessage();
                    Tester.UnpauseMessage();
					Form.Fail("Timeout");
				}
                else if (Form.StateTime.Elapsed < 0.1)
                    return;

                if (TxMessageTimer.Elapsed > 0.2)
                {
                    Form.SendMessage(TxMessage);
                    TxMessageTimer.Reset();
                }

                if (!FirstMessageRx)
                {
                    RFMessageTimer.Reset();
                    return;
                }
                
                if (RFMessageCount > 15)
                {
                    State = State + 1;
                    DUTInputs.UnpauseMessage();
                    DUTOutputs.UnpauseMessage();
                    Tester.UnpauseMessage();
                }

                if (RFMessageTimer.Elapsed > 4.0)
                {
                    FailString = "RF Message Count = " + RFMessageCount.ToString();
                    DUTInputs.UnpauseMessage();
                    DUTOutputs.UnpauseMessage();
                    Tester.UnpauseMessage();
                    Form.Fail(FailString);
                }
            }

            public void OnRX(RxMessage message)
            {
                if (message.Length != 7)
                    return;

                if (message.Data[0] != 0x00)
                    return;

                for (int i = 1; i < message.Length; i++)
                {
                    if (message.Data[i] != TxMessage[i])
                        return;
                }

                FirstMessageRx = true;
                RFMessageCount++;
            }
        }

        class State_TurnOffRFDebug : State_DUT_Message
        {
            static byte[] tx = { 0x02, 0x00 };
            static byte[] rx = { 0x02, 0x00 };

            public State_TurnOffRFDebug()
                : base("Disabling RF debug", tx, rx) { }
        }
		
		class State_CheckHallEffectPower : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Checking LF, LR, RF, RR, FJ Hall Power On (12V)";
            }

            public void Task()
            {
                Tester.SetOutputs(TesterObject.FLAGS.DUT_CONTROL);
                DUTOutputs.SetOutputs(DUTOutputObject.FLAGS.HALL_EFFECT_POWER_CONTROL);

                if (Tester.StateValid && (Tester.State == TesterObject.DUT_Hall_Power_Flags))
                    State = State + 1;
                else if (Form.StateTime.Elapsed > DefaultTimeout)
                    Form.Fail("Timeout");
            }

            public void OnRX(RxMessage message)
            {
            }
        }

        class State_CheckHallEffectSignalsHI : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Checking all HF Signals On (12V)";
            }

            public void Task()
            {
                Tester.SetOutputs(TesterObject.FLAGS.DUT_CONTROL);
                DUTOutputs.SetOutputs(DUTOutputObject.FLAGS.HALL_EFFECT_POWER_CONTROL);

                if (DUTInputs.StateValid && (DUTInputs.State == (DUTInputObject.FLAGS.LF_HALL_FEEDBACK | DUTInputObject.FLAGS.LR_HALL_FEEDBACK | DUTInputObject.FLAGS.RF_HALL_FEEDBACK | DUTInputObject.FLAGS.RR_HALL_FEEDBACK | DUTInputObject.FLAGS.TJ_HALL_FEEDBACK_1 | DUTInputObject.FLAGS.TJ_HALL_FEEDBACK_2)))
                    State = State + 1;
                else if (Form.StateTime.Elapsed > DefaultTimeout)
                    Form.Fail("Timeout");
            }

            public void OnRX(RxMessage message)
            {
            }
        }
		
		class State_CheckHF : StateInterface
        {
            String str;
            TesterObject.FLAGS setFlags;
            DUTInputObject.FLAGS checkFlags;

            public State_CheckHF(String s, TesterObject.FLAGS setf, DUTInputObject.FLAGS checkf)
            {
                str = s;
                setFlags =setf;
                checkFlags = checkf;
            }

            public void OnStateEnter()
            {
                Form.History = str;
            }

            public void Task()
            {
                DUTInputObject.FLAGS flags = DUTInputObject.FLAGS.LF_HALL_FEEDBACK | DUTInputObject.FLAGS.LR_HALL_FEEDBACK | DUTInputObject.FLAGS.RF_HALL_FEEDBACK | DUTInputObject.FLAGS.RR_HALL_FEEDBACK | DUTInputObject.FLAGS.TJ_HALL_FEEDBACK_1 | DUTInputObject.FLAGS.TJ_HALL_FEEDBACK_2;
                flags &= ~checkFlags;

                Tester.SetOutputs(TesterObject.FLAGS.DUT_CONTROL | setFlags);
                DUTOutputs.SetOutputs(DUTOutputObject.FLAGS.HALL_EFFECT_POWER_CONTROL);

                if (DUTInputs.StateValid && (DUTInputs.State == flags))
                    State = State + 1;
                else if (Form.StateTime.Elapsed > DefaultTimeout)
                    Form.Fail("Timeout");
            }

            public void OnRX(RxMessage message)
            {
            }

        }
		
		class State_CheckLFHF : State_CheckHF
        {
            public State_CheckLFHF() : 
                base("Checking LF HF Off (0V)", TesterObject.FLAGS.LF_HF_CONTROL, DUTInputObject.FLAGS.LF_HALL_FEEDBACK) { }
        }

        class State_CheckRFHF : State_CheckHF
        {
            public State_CheckRFHF() :
                base("Checking RF HF Off (0V)", TesterObject.FLAGS.RF_HF_CONTROL, DUTInputObject.FLAGS.RF_HALL_FEEDBACK) { }
        }

        class State_CheckLRHF : State_CheckHF
        {
            public State_CheckLRHF() :
                base("Checking LR HF Off (0V)", TesterObject.FLAGS.LR_HF_CONTROL, DUTInputObject.FLAGS.LR_HALL_FEEDBACK) { }
        }

        class State_CheckRRHF : State_CheckHF
        {
            public State_CheckRRHF() :
                base("Checking RR HF Off (0V)", TesterObject.FLAGS.RR_HF_CONTROL, DUTInputObject.FLAGS.RR_HALL_FEEDBACK) { }
        }
		
		class State_CheckTJHF1 : State_CheckHF
        {
            public State_CheckTJHF1() :
                base("Checking TJ HF1 Off (0V)", TesterObject.FLAGS.TJ_HF1_CONTROL, DUTInputObject.FLAGS.TJ_HALL_FEEDBACK_1) { }
        }
		
		class State_CheckTJHF2 : State_CheckHF
        {
            public State_CheckTJHF2() :
                base("Checking TJ HF2 Off (0V)", TesterObject.FLAGS.TJ_HF2_CONTROL, DUTInputObject.FLAGS.TJ_HALL_FEEDBACK_2) { }
        }
		
		class State_TestMotor : StateInterface
        {
            String jack;
            String direction;
            DUTOutputObject.FLAGS setFlags;
            TesterObject.FLAGS checkFlags;

            public State_TestMotor(String j, String d, DUTOutputObject.FLAGS set, TesterObject.FLAGS check)
            {
                jack = j;
                direction = d;
                setFlags = set;
                checkFlags = check;
            }
            
            public void OnStateEnter()
            {
                Form.History = "Checking " + jack + " Motor " + direction;
            }

            public void Task()
            {
                double shunt = 0;                
                
                Tester.SetOutputs(TesterObject.FLAGS.DUT_CONTROL);
                DUTOutputs.SetOutputs(DUTOutputObject.FLAGS.COIL_CONTROL | setFlags);

                if (Form.StateTime.Elapsed < RelayTestDelay)
                    return;

                switch (jack)
                {
                case "LF": shunt = DUTInputs.LFShunt; break;
                case "RF": shunt = DUTInputs.RFShunt; break;
                case "LR": shunt = DUTInputs.LRShunt; break;
                case "RR": shunt = DUTInputs.RRShunt; break;
                case "F": shunt = DUTInputs.FShunt; break;
                }
                
                if (Tester.StateValid
                    && DUTInputs.StateValid
                    && Tester.State == (TesterObject.DUT_Power_Flags | checkFlags)
                    && shunt > MinShuntCurrentRelayOn
                    && shunt < MaxShuntCurrentRelayOn)
                    State = State + 1;
                else if (Form.StateTime.Elapsed > DefaultTimeout)
                    Form.Fail("Timeout - Last read shunt value: " + shunt);
            }

            public void OnRX(RxMessage message)
            {
            }
        }
		
		class State_TestLFMotorExt : State_TestMotor
        {
            public State_TestLFMotorExt()
                : base("LF", "+", DUTOutputObject.FLAGS.LF_EXT_CONTROL, TesterObject.FLAGS.LFp) { } 
        }

        class State_TestLFMotorRet : State_TestMotor
        {
            public State_TestLFMotorRet()
                : base("LF", "-", DUTOutputObject.FLAGS.LF_RET_CONTROL, TesterObject.FLAGS.LFm) { } 
        }

        class State_TestRFMotorExt : State_TestMotor
        {
            public State_TestRFMotorExt()
                : base("RF", "+", DUTOutputObject.FLAGS.RF_EXT_CONTROL, TesterObject.FLAGS.RFp) { }
        }

        class State_TestRFMotorRet : State_TestMotor
        {
            public State_TestRFMotorRet()
                : base("RF", "-", DUTOutputObject.FLAGS.RF_RET_CONTROL, TesterObject.FLAGS.RFm) { }
        }
        
        class State_TestLRMotorExt : State_TestMotor
        {
            public State_TestLRMotorExt()
                : base("LR", "+", DUTOutputObject.FLAGS.LR_EXT_CONTROL, TesterObject.FLAGS.LRp) { }
        }

        class State_TestLRMotorRet : State_TestMotor
        {
            public State_TestLRMotorRet()
                : base("LR", "-", DUTOutputObject.FLAGS.LR_RET_CONTROL, TesterObject.FLAGS.LRm) { }
        }

        class State_TestRRMotorExt : State_TestMotor
        {
            public State_TestRRMotorExt()
                : base("RR", "+", DUTOutputObject.FLAGS.RR_EXT_CONTROL, TesterObject.FLAGS.RRp) { }
        }

        class State_TestRRMotorRet : State_TestMotor
        {
            public State_TestRRMotorRet()
                : base("RR", "-", DUTOutputObject.FLAGS.RR_RET_CONTROL, TesterObject.FLAGS.RRm) { }
        }
		
		class State_TestFMotorExt : State_TestMotor
        {
            public State_TestFMotorExt()
                : base("F", "+", DUTOutputObject.FLAGS.F_EXT_CONTROL, TesterObject.FLAGS.TJm) { }       // arh - note, the TJ motor is opposite all others in regards to EXT/RET vs -/+
        }

        class State_TestFMotorRet : State_TestMotor
        {
            public State_TestFMotorRet()
                : base("F", "-", DUTOutputObject.FLAGS.F_RET_CONTROL, TesterObject.FLAGS.TJp) { }       // arh - note, the TJ motor is opposite all others in regards to EXT/RET vs -/+
        }
        
        class State_TurnOffMotors : StateInterface
        {
            public void OnStateEnter()
            {
                DUTOutputs.SetOutputs(DUTOutputObject.FLAGS.None);
            }

            public void Task()
            {
                State = State + 1;
            }

            public void OnRX(RxMessage message)
            {
            }
        }

        class State_WaitForWrite : StateInterface
        {
            public void OnStateEnter()
            {
            }

            public void Task()
            {
                if (Form.StateTime.Elapsed > ProgrammingDelay)
                    State = State + 1;
            }

            public void OnRX(RxMessage message)
            {
            }
        }
		
		 class State_WriteFrontXZero : State_DUT_Message
        {
            static byte[] tx = { 0x30, 0x7F, 0xFF };
            static byte[] rx = { 0x30, 0x7F, 0xFF };
            public State_WriteFrontXZero()
                : base("Writing Front XZero = $7FFF", tx, rx)
            {
            }
        }

        class State_WriteFrontYZero : State_DUT_Message
        {
            static byte[] tx = { 0x38, 0x7F, 0xFF };
            static byte[] rx = { 0x38, 0x7F, 0xFF };
            public State_WriteFrontYZero()
                : base("Writing Front YZero = $7FFF", tx, rx)
            {
            }
        }

        class State_WriteRearXZero : State_DUT_Message
        {
            static byte[] tx = { 0x32, 0x7F, 0xFF };
            static byte[] rx = { 0x32, 0x7F, 0xFF };
            public State_WriteRearXZero()
                : base("Writing Rear XZero = $7FFF", tx, rx)
            {
            }
        }

        class State_WriteRearYZero : State_DUT_Message
        {
            static byte[] tx = { 0x3A, 0x7F, 0xFF };
            static byte[] rx = { 0x3A, 0x7F, 0xFF };
            public State_WriteRearYZero()
                : base("Writing Rear YZero = $7FFF", tx, rx)
            {
            }
        }

        class State_WriteHookupAngle : State_DUT_Message
        {
            static byte[] tx = { 0x51, 0x7F, 0xFF };
            static byte[] rx = { 0x51, 0x7F, 0xFF };
            public State_WriteHookupAngle()
                : base("Writing Hookup Angle = $7FFF", tx, rx)
            {
            }
        }

        class State_WriteZeroValid : State_DUT_Message
        {
            static byte[] tx = { 0x50, 0x00 };
            static byte[] rx = { 0x50, 0x00 };
            public State_WriteZeroValid()
                : base("Writing ZeroValid = $00", tx, rx)
            {
            }
        }

        class State_SetJackShippingState : State_DUT_Message
        {
            static byte[] tx = { 0x69 };
            static byte[] rx = { 0x69 };
            public State_SetJackShippingState()
                : base("Writing Jack Shipping State", tx, rx)
            {
            }
        }

        class State_ClearProductionByte : State_DUT_Message
        {
            static byte[] tx = { 0xF9, 0x00, 0x00 };
            static byte[] rx = { 0xF9, 0x00, 0x00 };

            public State_ClearProductionByte()
                : base("Setting production byte to 0x00", tx, rx) { }
        }
		
		class State_PowerDown : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Powering down";
            }

            public void Task()
            {
                Tester.SetOutputs(TesterObject.FLAGS.None);
                if (Tester.StateValid && Tester.State == TesterObject.FLAGS.EXT_BUTTON)
                    State = State + 1;
                else if (Form.StateTime.Elapsed > DefaultTimeout)
                    Form.Fail("Timeout");
            }

            public void OnRX(RxMessage message)
            {
            }
        }

        class State_PowerDownDelay : StateInterface
        {
            public void OnStateEnter()
            {
                Form.History = "Powering down";
            }

            public void Task()
            {
                if (Form.StateTime.Elapsed > PowerDownDelay)
                    State = State + 1;
                
            }

            public void OnRX(RxMessage message)
            {
            }
        }
		
        class State_VerifyFrontXZero : State_DUT_Message
        {
            static byte[] tx = { 0x30 };
            static byte[] rx = { 0x30, 0x7F, 0xFF };
            public State_VerifyFrontXZero()
                : base("Verifying Front XZero = $7FFF", tx, rx)
            {
            }
        }

        class State_VerifyFrontYZero : State_DUT_Message
        {
            static byte[] tx = { 0x38 };
            static byte[] rx = { 0x38, 0x7F, 0xFF };
            public State_VerifyFrontYZero()
                : base("Verifying Front YZero = $7FFF", tx, rx)
            {
            }
        }

        class State_VerifyRearXZero : State_DUT_Message
        {
            static byte[] tx = { 0x32 };
            static byte[] rx = { 0x32, 0x7F, 0xFF };
            public State_VerifyRearXZero()
                : base("Verifying Rear XZero = $7FFF", tx, rx)
            {
            }
        }

        class State_VerifyRearYZero : State_DUT_Message
        {
            static byte[] tx = { 0x3A };
            static byte[] rx = { 0x3A, 0x7F, 0xFF };
            public State_VerifyRearYZero()
                : base("Verifying Rear YZero = $7FFF", tx, rx)
            {
            }
        }

        class State_VerifyHookupAngle : State_DUT_Message
        {
            static byte[] tx = { 0x51 };
            static byte[] rx = { 0x51, 0x7F, 0xFF };
            public State_VerifyHookupAngle()
                : base("Verifying Hookup Angle = $7FFF", tx, rx)
            {
            }
        }

        class State_VerifyZeroValid : State_DUT_Message
        {
            static byte[] tx = { 0x50 };
            static byte[] rx = { 0x50, 0x00 };
            public State_VerifyZeroValid()
                : base("Verifying ZeroValid = $00", tx, rx)
            {
            }
        }
		
		class State_CheckProductionByte : State_DUT_Message
        {
            static byte[] tx = { 0xF9, 0x00 };
            static byte[] rx = { 0xF9, 0x00, 0x00 };

            public State_CheckProductionByte()
                : base("Checking production byte = 0x00", tx, rx) { }
        }
        
        class State_Pass : StateInterface
        {
            public void OnStateEnter()
            {
                Tester.Passed = true;
                Form.History = "Passed";
                State = State + 1;
            }

            public void Task()
            {
            }

            public void OnRX(RxMessage message)
            {
            }
        }
        
        class State_EndTest : StateInterface
        {
            public void OnStateEnter()
            {
            }

            public void Task()
            {
                Tester.SetOutputs(TesterObject.FLAGS.None);
                if (Form.StateTime.Elapsed > 0.5)
                    State = State + 1;
            }

            public void OnRX(RxMessage message)
            {
            }
        }
        
        class State_UpdateLogs : StateInterface
        {
            public void OnStateEnter()
            {
                Tester.PassFailLogUpdated = false;
                Tester.MasterPassLogUpdated = false;
            }

            public void Task()
            {
                if (!Tester.MasterPassLogUpdated)
                {
                    string log = DUTInputs.SerialNumber;
                    Tester.MasterPassLogUpdated = Form.UpdateMasterLogFile(log, Tester.Passed);
                }
                if (!Tester.PassFailLogUpdated)
                    Tester.PassFailLogUpdated = Form.UpdateLogFile(Tester.Passed);

                if (Tester.MasterPassLogUpdated && Tester.PassFailLogUpdated)
                    State = State + 1;
                else if (Form.StateTime.Elapsed >= 10.0)
                {
                    Tester.Passed = false;
                    string f = "";
                    if (!Tester.MasterPassLogUpdated)
                        f = "Error updating master pass log. ";
                    if (!Tester.PassFailLogUpdated)
                        f += "Error updating pass fail log";
                    Form.Fail(f);
                }
            }

            public void OnRX(RxMessage message)
            {
            }
        }

        class State_ShowResults : StateInterface
        {
            public void OnStateEnter()
            {
                if (Tester.Passed)
                {
                    Tester.SetOutputs(TesterObject.FLAGS.None);
                    Form.StatusIconLabel.Text = PassIcon;
                    Form.StatusIconLabel.ForeColor = Color.Lime;
                    Form.StatusIconLabel.Visible = true;
                    Form.StatusIconLabel.BringToFront();
                    Form.StatusBar.BackColor = Color.Lime;
                    Form.StatusBar.Visible = true;
                    Form.background = Color.Lime;

                }
                else
                {
                    Tester.SetOutputs(TesterObject.FLAGS.None);
                    Form.StatusIconLabel.Text = FailIcon;
                    Form.StatusIconLabel.ForeColor = Color.Red;
                    Form.StatusIconLabel.Visible = true;
                    Form.StatusIconLabel.BringToFront();
                    Form.StatusBar.BackColor = Color.Red;
                    Form.StatusBar.Visible = true;
                    Form.background = Color.Red;

                }
                mState = STATE.Ready;
            }

            public void Task()
            {
            }

            public void OnRX(RxMessage message)
            {
            }
        }        
            
        
///////////////////////////////////
///////////////////////////////////
///////////////////////////////////
///////////////////////////////////

        void OnAssemblyBarCodeScanned(String match, String text)
        {
            DUTInputs.SerialNumber = text;
 
            if (State == STATE.Ready)
            {
                if (!File.Exists(Form.MasterPassLogFileName))
                    MessageBox.Show("Could not access master pass log: " + MasterPassLogFileName, "File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (text.Substring(0, 6) == AssemblyNumber)
                {

                    State = STATE.Start;
                }

            }
        }

        void OnTesterBarCodeScanned(String match, String text)
        {
            if (State == STATE.GetTesterBarCode && text.Length == 16)
            {
                if (text.Contains(TesterPartNumber))
                {
                    Form.TesterBarCode = text;
                }
            }
        }

        void DoState()
        {
            // keep tester I/O up to date
            if (State != STATE.ComPortClosed)
            {
                if (DUTOutputs.TimeToSendMessage)
                    RS232_Send(DUTOutputs.OutgoingMessage);
                if (DUTInputs.TimeToSendMessage)
                    RS232_Send(DUTInputs.OutgoingMessage);
                if (Tester.TimeToSendMessage)
                    RS232_Send(Tester.OutgoingMessage);
            }

            // execute the appropriate 
            StateInterface i;
            if (Dictionary.TryGetValue(State, out i))
                i.Task();
        }
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            String s = "";
            
            // pump the state machine
            DoState();

            // display tester status
            if (!Tester.StateValid)
                Form.TesterStatusLabel.Text = "";
            else
            {
            	s = "DUT_CONTROL TX_ENABLE BUZZER FSVB RSVB LFHP RFHP LRHP RRHP T1HP T2HP LF+ LF- RF+ RF- LR+ LR- RR+ RR- TJ+ TJ-";
                s += "\n";
                if (Tester.FlagsSet(TesterObject.FLAGS.DUT_CONTROL)) s += "1           "; else s += "0           ";
                if (Tester.FlagsSet(TesterObject.FLAGS.TX_ENABLE)) s += "1         "; else s += "0         ";
                if (Tester.FlagsSet(TesterObject.FLAGS.BUZZER)) s += "1      "; else s += "0      ";
                if (Tester.FlagsSet(TesterObject.FLAGS.FrontSensorVBatt)) s += "1    "; else s += "0    ";
                if (Tester.FlagsSet(TesterObject.FLAGS.RearSensorVBatt)) s += "1    "; else s += "0    ";
                if (Tester.FlagsSet(TesterObject.FLAGS.LFHallPower)) s += "1    "; else s += "0    ";
                if (Tester.FlagsSet(TesterObject.FLAGS.RFHallPower)) s += "1    "; else s += "0    ";
                if (Tester.FlagsSet(TesterObject.FLAGS.LRHallPower)) s += "1    "; else s += "0    ";
                if (Tester.FlagsSet(TesterObject.FLAGS.RRHallPower)) s += "1    "; else s += "0    ";
                if (Tester.FlagsSet(TesterObject.FLAGS.TJHall1Power)) s += "1    "; else s += "0    ";
                if (Tester.FlagsSet(TesterObject.FLAGS.TJHall2Power)) s += "1    "; else s += "0    ";
                if (Tester.FlagsSet(TesterObject.FLAGS.LFp)) s += "1   "; else s += "0   ";
                if (Tester.FlagsSet(TesterObject.FLAGS.LFm)) s += "1   "; else s += "0   ";
                if (Tester.FlagsSet(TesterObject.FLAGS.RFp)) s += "1   "; else s += "0   ";
                if (Tester.FlagsSet(TesterObject.FLAGS.RFm)) s += "1   "; else s += "0   ";
                if (Tester.FlagsSet(TesterObject.FLAGS.LRp)) s += "1   "; else s += "0   ";
                if (Tester.FlagsSet(TesterObject.FLAGS.LRm)) s += "1   "; else s += "0   ";
                if (Tester.FlagsSet(TesterObject.FLAGS.RRp)) s += "1   "; else s += "0   ";
                if (Tester.FlagsSet(TesterObject.FLAGS.RRm)) s += "1   "; else s += "0   ";
                if (Tester.FlagsSet(TesterObject.FLAGS.TJp)) s += "1   "; else s += "0   ";
                if (Tester.FlagsSet(TesterObject.FLAGS.TJm)) s += "1   "; else s += "0   ";
                
                Form.TesterStatusLabel.Text = s;
            }
            
            // display DUT status
            if (!DUTInputs.StateValid)
                Form.DUTStatusLabel.Text = "";
            else
            {
				Form.DUTStatusLabel.Text = "DUT $" + DUTInputs.State.ToString("X")
                    + "  VBatt=" + DUTInputs.VBatt.ToString("0.0V")
                    + "  LFShunt=" + DUTInputs.LFShunt.ToString("0.0A")
                    + "  RFShunt=" + DUTInputs.RFShunt.ToString("0.0A")
                    + "  LRShunt=" + DUTInputs.LRShunt.ToString("0.0A")
                    + "  RRShunt=" + DUTInputs.RRShunt.ToString("0.0A")
                    + "  FShunt=" + DUTInputs.FShunt.ToString("0.0A");
            }
        }

        private void UserButton_Click(object sender, EventArgs e)
        {
            switch (State)
            {
            default:
                break;                    
            
            case STATE.CheckICTCycleCount:
                Form.ICTCycleCountMax += ICTCycleCountMaxWarningRepeat;
                HideUserButton();
                State = State + 1;
                break;
            
            case STATE.IncrementICTCycleCount:
                HideUserButton();
                State = State + 1;
                break;

            case STATE.WaitForStartButton:
                HideUserButton();
                State = STATE.IncrementICTCycleCount;			
                break;
            
            }
        }

        private void FailureDialog(string caption, string msgText)
        {
            Form.Result = DialogResult.None;
            MessageBoxButtons button = MessageBoxButtons.OK;
            MessageBoxIcon icon = MessageBoxIcon.Stop;

            // Display message box
            Form.Result = MessageBox.Show(msgText, caption, button, icon, MessageBoxDefaultButton.Button2);
        }

        private void NoticeDialogYesNo(string caption, string msgText, STATE YesState, STATE NoState)
        {
            Form.Result = DialogResult.None;
            MessageBoxButtons button = MessageBoxButtons.YesNo;
            MessageBoxIcon icon = MessageBoxIcon.Stop;

            // Display message box
            Form.Result = MessageBox.Show(msgText, caption, button, icon, MessageBoxDefaultButton.Button2);
            if (Form.Result == DialogResult.Yes)
            {
                State = YesState;
            }
            else
            {
                State = NoState;
            }

        }

        void FailureAckDialog(string caption, string msgText)
        {

            //prevent message box from stalling timer/message pump
            Form.BeginInvoke(((MethodInvoker)delegate { FailureDialog(caption, msgText); }));
        }
        
        bool GetDataFromDataBase()
        {
            bool IsNew;
            string ErrorMessage;
            bool result;

            result = LincTabDB.LincTabDB.GetMacAddress(DUTInputs.SerialNumber, "PC Tool - " + IDS.AppInfo.BuildDate.ToString(), out DUTInputs.MACaddr, out IsNew, out ErrorMessage);

            if (result)
            {
                //Keep macaddr in big endian form
                Array.Reverse(DUTInputs.MACaddr, 0, DUTInputs.MACaddr.Length);

                if (State == STATE.GetDataFromDatabaseWait)
                    State = State + 1;
            }
            else
            {
                Fail("Database error: " + ErrorMessage);
            }
            return result;
        }

        void SendMessage(byte[] message)
        {
            Form.MessageTime.Reset();
            Form.RS232_Send(message);
        }

        void Fail()
        {
            String failure = HistoryBox.Items[HistoryBox.Items.Count - 1].ToString();

            HideUserButton();
            // dump tester status
            Form.History = "Failed:" + failure;
            Form.History = "     Tester = $" + Tester.State.ToString("X") + " " + Tester.String;
            Form.History = "     DUT = $" + DUTInputs.State.ToString("X") + " " + DUTInputs.String;
            Form.StatusLabel.Text = "Failed: " + failure;
            if (State == STATE.UpdateLogs)
                State = STATE.ShowResults;
            else
                State = STATE.EndTest;
            FailureAckDialog("DUT Failed Testing", "Select OK to Continue");
        }

        void Fail(String f)
        {
            HideUserButton();
            Form.History = "Failed: " + f + " (" + State.ToString() + ")";
            Form.StatusLabel.Text = "";
            Form.StatusLabel.BackColor = Color.Red;
            Form.StatusLabel.ForeColor = Color.Black;
            Form.StatusLabel.Text = "Failed: " + f + " (" + State.ToString() + ")";
            if (State == STATE.UpdateLogs)
                State = STATE.ShowResults;
            else
                State = STATE.EndTest;

            FailureAckDialog("DUT Failed Testing", "Select OK to Continue");


        }
        
        void ShowUserButton(string s)
        {
            Form.UserButton.Text = s;
            Form.UserButton.Visible = true;
            Form.UserButton.Enabled = true;
            Form.UserButton.BringToFront();
        }

        void HideUserButton()
        {
            Form.UserButton.Visible = false;
        }

        private void Form1_RS232_Closed()
        {
            State = STATE.ComPortClosed;
            Form.PortStatusLabel.Text = "";
        }

        private void Form1_RS232_Opened(int portnumber, int baudrate)
        {
            State = (baudrate == TesterBaudRate) ? STATE.GetTesterBarCode : STATE.ComPortClosed;
            Form.PortStatusLabel.Text = "COM" + portnumber.ToString() + ", " + baudrate.ToString() + " baud";
        }

        private void Form1_RS232_Rx(RxMessage message)
        {
            if (message.Echo)
                return;

            // kick start the state
            StateInterface i;
            if (Dictionary.TryGetValue(State, out i))
                i.OnRX(message);

            if (message.Length < 2)
                return;	

            switch (message.Data[0])
            {
				case 0x00:
					// I/O status
                    if (message.Data[1] != 0xF9)
                        break;    
                    
                    switch (message.Data[2])
                    {
                    case 0x10:
                        // input status
                        if (message.Length == 10 + 1)
                            DUTInputs.MessageIn(message.Data[3], message.Data[4], message.Data[5], message.Data[6], message.Data[7], message.Data[8], message.Data[9], message.Data[10]);
                        break;
                                    
                    case 0x13:
                        // output control status
                        if (message.Length == 4 + 1)
                            DUTOutputs.MessageIn(message.Data[3], message.Data[4]);
                        break;
                    }
                    break;

				case 0x10:
					// tester status
					if (message.Length == 5 + 1)
					{
						Tester.MessageIn(message.Data[1], message.Data[2], message.Data[3], message.Data[4]);
						DoState();
					}
					break;
            }
        }
        
        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder s = new StringBuilder();
            foreach (String t in Form.HistoryBox.Items)
                s.AppendLine(t);
            if (s.Length > 0)
                Clipboard.SetText(s.ToString());
        }

        bool UpdateLogFile(bool passed)
        {
            String filename = Form.PassFailLogBaseFileName + " " + TestTime.Year.ToString("0000") + TestTime.Month.ToString("00") + TestTime.Day.ToString("00") + ".txt";

            // see if the file exists
            bool write_header = !File.Exists(filename);

            try
            {
                // create/append to the file
                using (FileStream stream = new FileStream(filename, FileMode.Append, FileAccess.Write, FileShare.None))
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        // create/append to the file

                        string tab = "\t";

                        // write header
                        //Failed	Time	    Serial number	    Test result
                        //          15:25:30	18448C0123456789	Passed
                        if (write_header)
                        {
                            string h = "Failed";
							h += tab + "Time";
							h += tab + "Serial number";
							h += tab + "Test result";
							writer.WriteLine(h);
                        }
						
						string log = passed ? " " : "*";
						log += tab + TestTime.Hour.ToString("00") + ":" + TestTime.Minute.ToString("00") + ":" + TestTime.Second.ToString("00");
						log += tab + DUTInputs.SerialNumber;
						log += tab + Form.StatusLabel.Text;
						writer.WriteLine(log);

                        writer.Close();
                    }
                    stream.Close();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Add/Remove text in the master log file
        /// </summary>
        /// <param name="text">text to add</param>
        /// <param name="addtofile">true if text should be added, false if text should be removed</param>

        bool CheckMasterLogFile(String text)
        {
            string filename = Form.MasterPassLogFileName;
            String filetxt = "";

            try
            {
                // using {} statement releases resources immediately, rather than waiting for the garbage collector to get around to it
                using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        filetxt = reader.ReadToEnd();
                        reader.Close();
                    }
                    stream.Close();
                }
            }
            catch
            {
                return false;
            }

            // does the line exist in the file?
            text += Environment.NewLine; // append newline
            bool infile = Regex.IsMatch(filetxt, text);

            if (infile)
            {
                return true; // already added
            }
            else
            {
                return false;
            }

            
        }

        /// <summary>
        /// Add/Remove text in the master log file
        /// </summary>
        /// <param name="text">text to add</param>
        /// <param name="addtofile">true if text should be added, false if text should be removed</param>
 
        bool UpdateMasterLogFile(String text, bool add)
        {
            string filename = Form.MasterPassLogFileName;
            String filetxt = "";

            try
            {
                // using {} statement releases resources immediately, rather than waiting for the garbage collector to get around to it
                using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        filetxt = reader.ReadToEnd();
                        reader.Close();
                    }
                    stream.Close();
                }
            }
            catch
            {
                return false;
            }

            // does the line exist in the file?
            text += Environment.NewLine; // append newline
            bool infile = Regex.IsMatch(filetxt, text);

            if (add == infile)
                return true; // already added/deleted

            // add/remove the line
            if (add)
                filetxt += text;
            else
                filetxt = Regex.Replace(filetxt, text, "");

            try
            {
                File.Copy(filename, filename + ".bak", true);
                //need to ensure all lines are removed to avoid case of deleting from middle of log which leaves last 
                //number in file 2 times.
                using (FileStream fileStream = new FileStream(filename, FileMode.Truncate, FileAccess.Write, FileShare.None))
                {
                    using (TextWriter writer = new StreamWriter(fileStream))
                    {
                        writer.Write(filetxt);
                        writer.Close();
                    }
                    fileStream.Close();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        int GetICTCycleCount()
        {
            string filename = Form.ICTCycleCountLogFileName;
            int count = 0;
            String filetxt = "";
            bool found = false;

            try
            {

                // using {} statement releases resources immediately, rather than waiting for the garbage collector to get around to it
                using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        filetxt = reader.ReadToEnd();
                        reader.Close();
                    }
                    stream.Close();
                }
            }
            catch
            {
                return -1;       // problem accessing/reading file
            }
            

            using (StringReader sreader = new StringReader(filetxt))
            {
                string line;
                while (!found && (line = sreader.ReadLine()) != null)
                {
                    line.Replace(" ", "");
                    if (line.Length >= 8)
                    {
                        if (line.Substring(0, 17) == (Form.TesterBarCode + "="))
                        {
                            found = true;
                            count = int.Parse(line.Substring(17, line.Length - 17));
                        }
                    }
                }
                sreader.Close();
            }

            if (!found)
                return -1;       // could not find PID in log

            if (count >= 0)
                return count;
            else
                return -1;
        }

        int IncrementICTCycleCount()
        {
            string filename = Form.ICTCycleCountLogFileName;
            int count = 0;
            String filetxt = "";
            string currentline = null, updatedline;
            bool found = false;

            try
            {
                // using {} statement releases resources immediately, rather than waiting for the garbage collector to get around to it
                using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        filetxt = reader.ReadToEnd();
                        reader.Close();
                    }
                    stream.Close();
                }
            }
            catch
            {
                return -1;       // problem accessing/reading file
            }

            using (StringReader sreader = new StringReader(filetxt))
            {
                while (!found && (currentline = sreader.ReadLine()) != null)
                {
                    currentline.Replace(" ", "");
                    if (currentline.Length >= 16)
                    {
                        if (currentline.Substring(0, 17) == (Form.TesterBarCode + "="))
                        {
                            found = true;
                            count = int.Parse(currentline.Substring(17, currentline.Length - 17));
                        }
                    }
                }
                sreader.Close();
            }

            if (!found)
                return -1;       // could not find PID in log
            else
            {
                count++;
                updatedline = Form.TesterBarCode + "=" + count.ToString();
                filetxt = filetxt.Replace(currentline, updatedline);
                try
                {
                    using (FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Write, FileShare.None))
                    {
                        using (TextWriter writer = new StreamWriter(fileStream))
                        {
                            writer.Write(filetxt);
                            writer.Close();
                        }
                        fileStream.Close();
                    }
                    return count;
                }
                catch
                {
                    return -1;      // problem accessing/writing file
                }
            }
        }

        bool ResetICTCycleCount()
        {
            string filename = Form.ICTCycleCountLogFileName;
            String filetxt = "";
   
            try
            {
                // using {} statement releases resources immediately, rather than waiting for the garbage collector to get around to it
                using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string line = null;
                        while ((line = reader.ReadLine()) != null)
                        {
                            line.Replace(" ", "");
                            if (line.Length >= 5)
                            {
                                if (line.Substring(0, 16) != Form.TesterBarCode)
                                    filetxt += line + Environment.NewLine;    // copy this line copy to filetxt (keep)
                            }
                        }
                        reader.Close();
                    }
                    stream.Close();
                }
            }
            catch
            {
                return false;       // problem accessing/reading file
            }
            

            filetxt += Form.TesterBarCode + "=0" + Environment.NewLine;
            try
            {
                using (FileStream fileStream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    using (TextWriter writer = new StreamWriter(fileStream))
                    {
                        writer.Write(filetxt);
                        writer.Close();
                    }
                    fileStream.Close();
                }
                return true;
            }
            catch
            {
                return false;      // problem accessing/writing file
            }
        }

        private void resetTesterCycleCountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Form.TesterBarCode == "")
                MessageBox.Show("Please scan tester's bar code then try again", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                DialogResult result = MessageBox.Show("Are you sure you want to reset tester " + TesterBarCode + "'s cycle count to 0? ", "File Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (!ResetICTCycleCount())
                        MessageBox.Show("Problem accessing ICT cycle count log.  Please inform the job leader if this warning continues to occur.", "File Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                        MessageBox.Show("Tester " + Form.TesterBarCode + "'s cycle count has been reset.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
