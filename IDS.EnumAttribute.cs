// IDS.EnumAttribute.cs
// Version 1.0
//
// History
// 1.0 -- first release
//
//
// Class that allows custom attributes to be associated with an enumeration
//
// example usage:
//
// define attribute class:
//   class CustomAttribute : IDS.EnumAttribute { public CustomAttribute(string n) : base(n) { } }
//
// define enumeration, and apply custom attribute
//   enum CustomEnum
//   {
//      [CustomAttribute("first enumerated value")]
//      Value1,
//      [CustomAttribute("second enumerated value")]
//      Value2,
//   }
//
// lookup the attribute for a specific enum
//   CustomEnum val = CustomEnum.Value1;
//   string attribute = IDS.EnumAttribute.Get(val, typeof(CustomAttribute));

using System;
using System.Reflection;

namespace IDS
{
    class EnumAttribute : Attribute
    {
        public object Attribute;

        public EnumAttribute(object attribute)
        {
            Attribute = attribute;
        }

        static public object Get(Enum _enum, Type attribute_type)
        {
            MemberInfo[] memInfo = _enum.GetType().GetMember(_enum.ToString());
            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(attribute_type, false);
                if (attrs != null && attrs.Length > 0)
                    return ((EnumAttribute)attrs[0]).Attribute;
            }
            return null;
        }

        static public String GetText(Enum _enum, Type attribute_type)
        {
            return (String)Get(_enum, attribute_type);
        }

        static public int GetInt(Enum _enum, Type attribute_type)
        {
            return (int)Get(_enum, attribute_type);
        }
    }
}
