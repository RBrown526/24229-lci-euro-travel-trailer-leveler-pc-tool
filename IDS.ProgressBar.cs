// IDS.ProgressBar.cs
// Version 1.0
//
// History
// 1.0 -- first release

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IDS
{
    public partial class ProgressBar : Control
    {
        float mMinimum = 0;	// Minimum value for progress range
        float mMaximum = 100;	// Maximum value for progress range
        float mValue = 0;		// Current progress
        Color mBarColor = Color.Blue;		// Color of progress meter

        [Description("Color of progress bar"), Category("Appearance")]
        public Color BarColor
        {
            get { return mBarColor; }
            set 
            { 
                if (mBarColor != value)
                {
                    mBarColor = value;
                    Invalidate(); 
                } 
            }
        }

        public new Color BackColor
        {
            get { return base.BackColor; }
            set
            {
                if (base.BackColor != value)
                {
                    base.BackColor = value;
                    Invalidate();
                }
            }
        }

        [Description("Maximum value of the bar"), Category("Behavior")]
        public float Minimum
        {
            get 
            {
                return mMinimum;
            }
            set
            {
                if (mMinimum != value)
                {
                    mMinimum = value;
                    this.Invalidate();
                }
            }
        }

        [Description("Minimum value of the bar"), Category("Behavior")]
        public float Maximum
        {
            get
            {
                return mMaximum;
            }
            set
            {
                if (mMaximum != value)
                {
                    mMaximum = value;
                    Invalidate();
                }
            }
        }

        [Description("Value of the bar"), Category("Behavior")]
        public float Value
        {
            get
            {
                return mValue;
            }

            set
            {
                if (mValue != value)
                {
                    mValue = value;
                    Invalidate();
                }
            }
        }

        public new String Text
        {
            get { return base.Text; }
            set
            {
                if (base.Text != value)
                {
                    base.Text = value;
                    Invalidate();
                }
            }
        }

        protected override void OnResize(EventArgs e)
        {
            // Invalidate the control to get a repaint.
            this.Invalidate();
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            // Calculate area for drawing the progress.
            float percent = 0;
            if (mMaximum > mMinimum)
            {
                percent = (mValue - mMinimum) / (mMaximum - mMinimum);
                if (percent < 0)
                    percent = 0;
                if (percent > 1)
                    percent = 1;
            }
            Rectangle client = this.ClientRectangle;
            Rectangle bar = client;
            bar.Width = (int)((float)bar.Width * percent);
            Rectangle back = client;
            back.Width -= bar.Width;
            back.X = bar.Left + bar.Width;

            String text = Text.Trim();
            bool drawtext = (text.Length > 0);
            Rectangle textrect = client;
            if (drawtext)
            {
                SizeF size = g.MeasureString(text, Font);
                textrect.Width = (int)size.Width;
                textrect.Height = (int)size.Height;
                textrect.X = (client.X + client.Width - textrect.Width) / 2;
                textrect.Y = (client.Y + client.Height - textrect.Height) / 2;
            }

            // Draw the progress meter.
            SolidBrush barbrush = new SolidBrush(BarColor);
            SolidBrush backbrush = new SolidBrush(BackColor);
            g.FillRectangle(backbrush, back);
            if (drawtext)
            {
                g.SetClip(back);
                g.DrawString(text, Font, barbrush, textrect);
                g.SetClip(client);
            }
            g.FillRectangle(barbrush, bar);
            if (drawtext)
            {
                g.SetClip(bar);
                g.DrawString(text, Font, backbrush, textrect);
                g.SetClip(client);
            }

            // Draw a three-dimensional border around the control.
            Draw3DBorder(g);

            // Clean up.
            backbrush.Dispose();
            barbrush.Dispose();
            g.Dispose();
        }

        private void Draw3DBorder(Graphics g)
        {
            int PenWidth = (int)Pens.White.Width;

            g.DrawLine(Pens.DarkGray,
                new Point(this.ClientRectangle.Left, this.ClientRectangle.Top),
                new Point(this.ClientRectangle.Width - PenWidth, this.ClientRectangle.Top));
            g.DrawLine(Pens.DarkGray,
                new Point(this.ClientRectangle.Left, this.ClientRectangle.Top),
                new Point(this.ClientRectangle.Left, this.ClientRectangle.Height - PenWidth));
            g.DrawLine(Pens.White,
                new Point(this.ClientRectangle.Left, this.ClientRectangle.Height - PenWidth),
                new Point(this.ClientRectangle.Width - PenWidth, this.ClientRectangle.Height - PenWidth));
            g.DrawLine(Pens.White,
                new Point(this.ClientRectangle.Width - PenWidth, this.ClientRectangle.Top),
                new Point(this.ClientRectangle.Width - PenWidth, this.ClientRectangle.Height - PenWidth));
        } 
    }
}
