﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("24229-A LCI Euro Travel Trailer Leveler ICT PC Tool")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Innovative Design Solutions, Inc.\r\n6801 Fifteen Mile Road\r\nSterling Hts., MI 48312\r\nwww.idselectronics.com")]
[assembly: AssemblyProduct("24229 LCI Euro Travel Trailer Leveler ICT PC Tool")]
[assembly: AssemblyCopyright("Copyright © 2016 Innovative Design Solutions, Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("20b3ef67-265b-4a55-9756-f78da0afdec8")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.3.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
