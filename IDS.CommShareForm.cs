// IDS.CommShareForm.cs
// Version 1.4
//
// History
// 1.0 -- first release
// 1.1 -- modified to work with IDS.Form class
// 1.2 -- modified to pass buffers with inherent length
// 1.3 -- buf fix
// 1.4 -- CommShare window now opened after our window is fully shown

using System;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices; // DLLImport
using System.Reflection;

namespace IDS
{
    public partial class CommShareForm : IDS.Form
    {
        // incoming message
        [StructLayout(LayoutKind.Sequential)]
        public struct RxMessage
        {
            public int Length;
            int EchoVal; // must read as int, and convert to bool manually
            public bool Echo { get { return (EchoVal & 0xFF) != 0; } }
            public double Time;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
            public byte[] Data;
        };

        // event callback when the port is opened
        public delegate void RS232_OpenedDelegate(int portnumber, int baudrate);
        [Category("Custom"), Description("Event raised when RS232 port is opened.")]
        public event RS232_OpenedDelegate RS232_Opened;

        public delegate void RS232_ClosedDelegate();
        [Category("Custom"), Description("Event raised when RS232 port is closed.")]
        public event RS232_ClosedDelegate RS232_Closed;

        public delegate void RS232_RxDelegate(RxMessage message);
        [Category("Custom"), Description("Event raised when a message is received on the RS232 port.")]
        public event RS232_RxDelegate RS232_Rx;

        // remember the port number
        private int mPortNumber = 0;
        public int PortNumber { get { return mPortNumber; } }

        // remember the baud rate
        private int mBaudRate = 0;
        public int BaudRate { get { return mBaudRate; } }

        // remember the port state
        private bool mPortOpen = false;
        public bool PortOpen { get { return mPortOpen; } }

        // handle to the actual CommShare window
        private IntPtr mCommShareHandle = IntPtr.Zero;
        private IntPtr CommShareHandle
        {
            get { if (DesignMode) return System.IntPtr.Zero; return mCommShareHandle; }
            set
            {
                if (mCommShareHandle != value)
                {
                    mCommShareHandle = value;
                    if (value == IntPtr.Zero)
                        OnPortClose();
                }
            }
        }

        // class to encapsulate access to Win32 functions
        private class Win32
        {
            public delegate bool EnumWindowsProc(IntPtr hWnd, int lParam); //delegate used for EnumWindows() callback function
            [DllImport("user32")] public static extern int EnumWindows(EnumWindowsProc callback, int lparam);

            [DllImport("user32")] public static extern int GetWindowText(IntPtr hWnd, StringBuilder title, int size);

            [DllImport("user32")] public static extern int GetWindowTextLength(IntPtr hWnd);

            [DllImport("user32")] public static extern bool IsWindow(IntPtr hWnd);

            [DllImport("user32")] public static extern int SendMessage(IntPtr hWnd, int uMsg, int wParam, int lParam);

            [DllImport("user32")] public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
            public const int SW_HIDE = 0;
            public const int SW_SHOWNORMAL = 1;
            public const int SW_SHOWMINIMIZED = 2;
            public const int SW_SHOWMAXIMIZED = 3;
            public const int SW_MAXIMIZE = 3;
            public const int SW_SHOWNOACTIVATE = 4;
            public const int SW_SHOW = 5;
            public const int SW_MINIMIZE = 6;
            public const int SW_SHOWMINNOACTIVE = 7;
            public const int SW_SHOWNA = 8;
            public const int SW_RESTORE = 9;
            public const int SW_SHOWDEFAULT = 10;
            public const int SW_FORCEMINIMIZE = 11;

            [DllImport("user32")] public static extern bool SetForegroundWindow(IntPtr hWnd);

            public const int WM_COPYDATA = 0x4A;
            [StructLayout(LayoutKind.Sequential)]
            public struct COPYDATASTRUCT
            {
                public int dwData;
                public int cbData;
                public IntPtr lpData;
            }
        }

        // used to get the address of a managed object when passing to Win32 functions
        private static IntPtr VarPtr(object e)
        {
            GCHandle GC = GCHandle.Alloc(e, GCHandleType.Pinned);
            int gc = GC.AddrOfPinnedObject().ToInt32();
            GC.Free();
            return (IntPtr)gc;
        }

        public CommShareForm()
        {
            InitializeComponent();
        }
        
        // EnumWindows callback function
        StringBuilder Caption = new StringBuilder(256);
        private bool EnumProc(IntPtr hWnd, int lParam)
        {
            // find the first window whose caption reads "IDS CommShare v"
            int len = Win32.GetWindowTextLength(hWnd);
            if (len < 15)
                return true; // keep looking
            Win32.GetWindowText(hWnd, Caption, 256);
            if (Caption.ToString().Substring(0, 15) != "IDS CommShare v")
                return true; // keep looking

            // window found - remember the handle
            IntPtr handle = (IntPtr)lParam; // recover handle
            CommShareForm form = (CommShareForm)CommShareForm.FromHandle(handle); // recover form
            form.CommShareHandle = hWnd;
                
            return false; // done
        }

        // used to locate the CommShare window
        private bool FindCommShare()
        {
            if (DesignMode)
                return false;

            if (CommShareHandle != IntPtr.Zero && Win32.IsWindow(CommShareHandle))
                return true;

            // try and find the window
            CommShareHandle = IntPtr.Zero;
            Win32.EnumWindowsProc ewp = new Win32.EnumWindowsProc(EnumProc);
            Win32.EnumWindows(ewp, (int)this.Handle);

            // did we find the window?
            IntPtr hwnd = CommShareHandle;
            if (hwnd == IntPtr.Zero)
                return false;

            // add our window to the message chain
            Win32.COPYDATASTRUCT cds;
            cds.dwData = (int)this.Handle;
            cds.cbData = 0;
            cds.lpData = IntPtr.Zero;
            Win32.SendMessage(hwnd, Win32.WM_COPYDATA, (int)this.Handle, (int)VarPtr(cds));

            return true;
        }

        private void CommShareForm_Shown(object sender, EventArgs e)
        {
            // locate CommShare when form is first shown
            FindCommShare();                    
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            // periodically check connection to CommShare
            if (Visible)
                FindCommShare();            
        }

        // attempts to connect to CommShare
        protected bool RS232_Connect()
        {
            if (FindCommShare())
		        return true;
            MessageBox.Show("Unable to locate IDS CommShare application.\nPlease load the application before continuing.", "Port configuration", MessageBoxButtons.OK, MessageBoxIcon.Error);
	        return false;
        }

        // opens the port configuration dialog
        protected bool RS232_ShowConfigDialog()
        {
            if (FindCommShare())
	        {
                Win32.ShowWindow(CommShareHandle, Win32.SW_SHOW);
                Win32.ShowWindow(CommShareHandle, Win32.SW_RESTORE);
                Win32.SetForegroundWindow(CommShareHandle);
		        return true;
	        }
            MessageBox.Show("Unable to locate IDS CommShare application.\nPlease load the application and try again.", "Port configuration", MessageBoxButtons.OK, MessageBoxIcon.Error);
	        return false;
        }

        // sends a message over the port
        protected void RS232_Send(byte size, byte[] message)
        {
            IntPtr hwnd = CommShareHandle;
            if (hwnd != IntPtr.Zero)
	        {
		        Win32.COPYDATASTRUCT cds;
		        cds.dwData = 0;
		        cds.cbData = size;
                cds.lpData = VarPtr(message);

                Win32.SendMessage(hwnd, Win32.WM_COPYDATA, (int)this.Handle, (int)VarPtr(cds));
	        }
        }

        protected void RS232_Send(byte[] message)
        {
            RS232_Send((byte)message.Length, message);
        }

        private void OnPortOpen(int portnum, int baudrate)
        {
            mPortNumber = portnum;
            mBaudRate = baudrate;
            mPortOpen = true;
            if (RS232_Opened != null)
                RS232_Opened(portnum, baudrate);
        }

        private void OnPortClose()
        {
            mPortOpen = false;
            mPortNumber = 0;
            mBaudRate = 0;
            if (RS232_Closed != null)
                RS232_Closed();
        }

        // MESSAGE_PORT_OPEN
        [StructLayout(LayoutKind.Sequential)]
        struct PortData
        {
            public int PortNum;
            public int BaudRate;
        };

        protected override void WndProc(ref Message message)
        {
            if (message.Msg == Win32.WM_COPYDATA)
            {
                Win32.COPYDATASTRUCT cds = (Win32.COPYDATASTRUCT)Marshal.PtrToStructure(message.LParam, typeof(Win32.COPYDATASTRUCT));

                switch (cds.dwData)
                {
                    case 0: // MESSAGE_PORT_CLOSED
                        OnPortClose();
                        break;
                    case 1: // MESSAGE_PORT_OPEN
                        PortData data = (PortData)Marshal.PtrToStructure(cds.lpData, typeof(PortData));
                        OnPortOpen(data.PortNum, data.BaudRate);
                        break;
                    case 2: // MESSAGE_DATA_RECEIVED
                        if (RS232_Rx != null)
                        {
                            RxMessage msg = (RxMessage)Marshal.PtrToStructure(cds.lpData, typeof(RxMessage));
                            RS232_Rx(msg);
                        }
                        break;
                }
                return;
            }
                
            base.WndProc(ref message);
        }
    }
}